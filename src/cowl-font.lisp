
(in-package #:cowl)

(defun blankp (char)
  (declare (type character char))
  (find char '(#\Space #\Newline #\Page #\Tab #\Backspace #\Return #\Linefeed)))

(defun string-split (string &optional (predicate #'blankp))
  "Split a string into a list of strings, partitioning by characters matching the predicate.
Default any whitespace character"
  (loop for i = 0 then (1+ j)
     as j = (position-if predicate string :start i)
     collect (subseq string i j)
     while j))

(defun font-string-cursor-offset (font string cursor-x)
  "Returns the pixels offset from left of a cursor in the string"
  (declare (type string string))
  (declare (type (integer 0) cursor-x))
  (loop for c across string for i from 0 until (= i cursor-x) summing (ftgl:get-font-advance font (make-string 1 :initial-element c))))

(defun font-string-character-position (font string offset-x)
  "Returns the integer character index in string at the offset-x"
  (declare (type string string))
  (declare (type (integer 0) offset-x))

  (do* ((i 0 (1+ i))
        (c-offset 0))
       ((or (>= i (length string))
            (> (+ c-offset (/ (ftgl:get-font-advance font (subseq string i (1+ i)))
                              2))
               offset-x))
        i)
    (incf c-offset (ftgl:get-font-advance font (subseq string i (1+ i))))))

(defun font-text-wrap (font text width)
  "Using a font, wraps a string of text to a given width.
Returns a list of strings of the lines and the height of all the strings in pixels.
   "
  (let* ((space-width (ftgl:get-font-advance font " "))
         lines
         lines-tail
         (line-width 0)
         (num-lines 0))
    (flet ((new-line ()
             (incf num-lines)
             (setf line-width 0)
             (let ((prev-tail lines-tail)) ; append to end, maintaining tail
               (setf lines-tail (cons (make-array 64 :element-type 'character :initial-element #\Nul :fill-pointer 0) nil))
               (when prev-tail
                 (setf (cdr prev-tail) lines-tail)))))
      (new-line)
      (setf lines lines-tail)         ; set the head to the first tail
      ;; collect up a list of (word . width)s one the split text
      (do ((word-word-width (loop for word in (string-split text) collecting (cons word (ftgl:get-font-advance font word)))
                            (cdr word-word-width)))
          ((null word-word-width)
           (values lines (* num-lines (ftgl:get-font-line-height font))))
        (destructuring-bind (word . word-width) (car word-word-width)
          ;; add word to line
          (loop for c across word do (vector-push-extend c (car lines-tail)))
          ;; add word-width to line
          (incf line-width word-width)
          ;; if there is a next word
          (when (cdr word-word-width) 
            (incf line-width space-width)
            (vector-push-extend #\Space (car lines-tail))
            ;; if puts us over the edge
            (if (> (+ line-width (cdadr word-word-width)) width)
                (new-line))))))))


(defun gl-print (font x y text)
  (declare (type string text))
  (gl:with-push-attrib (gl:+texture-bit+ gl:+depth-buffer-bit+)
    (gl:depth-mask gl:+false+)
    (gl:disable gl:+depth-test+)
    (gl:enable gl:+blend+)
    (gl:blend-func gl:+src-alpha+ gl:+one-minus-src-alpha+)

    (gl:with-push-matrix
      (gl:translate-f (coerce x 'single-float)
		      (coerce y 'single-float)
		      0.0)
      (ftgl:render-font font text :all))))