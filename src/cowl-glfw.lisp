(defpackage #:cowl-glfw
  (:use #:cl)
  (:export
   #:mouse-button-callback
   #:mouse-wheel-callback
   #:key-callback
   #:edit-mode-char-callback
   #:enter-edit-mode-hook
   #:leave-edit-mode-hook
   #:clear-input-events
   #:setup-input-callbacks
   #:teardown-input-callbacks))

(in-package #:cowl-glfw)

(pushnew 'cowl:delete-default-font cl-glfw:*terminate-hooks*)

(defun mouse-button-callback (button action)
  "Callback for capturing GLFW mouse button events and passing them to Cowl."
  (cowl:mouse-button-event button (eql action glfw:+press+)))

(defun key-callback (key action)
  "The key capture GLFW callback, note that this is not intended for 
capturing character input, use edit-mode-char-callback for that."
  ;; unless it's a character in edit mode.
  (unless (and (characterp key)
               (typep cowl:*focused-widget* 'cowl:input))
    (cowl:key-event key (eql action glfw:+press+))))

;;; for cowl:input boxes
(defun edit-mode-char-callback (character action)
  "Character capture GLFW callback. Note that this is not intended for capturing
non-textual and modifier keys, use key-callback for that."
  (cowl:key-event character (eql action glfw:+press+)))

(defun enter-edit-mode-hook ()
  "The cowl-glfw callback to set up GLFW for reading
textual input."
  (glfw:enable glfw:+key-repeat+)
  (glfw:set-char-callback #'edit-mode-char-callback))

(defun leave-edit-mode-hook ()
  "The cowl-glfw callback to undo enter-edit-mode-hook."
  (glfw:disable glfw:+key-repeat+)
  (glfw:set-char-callback nil))

(defun setup-input-callbacks ()
  "Set the callbacks for Cowl on GLFW."
  (glfw:set-mouse-button-callback 'mouse-button-callback)
  (glfw:set-mouse-pos-callback 'cowl:mouse-motion-event)
  (glfw:set-mouse-wheel-callback 'cowl:mouse-wheel-event)
  (glfw:set-key-callback 'key-callback)
  (pushnew 'enter-edit-mode-hook cowl:*enter-edit-mode-hooks*)
  (pushnew 'leave-edit-mode-hook cowl:*leave-edit-mode-hooks*))

(defun clear-input-events ()
  "Forces event collection with the cowl:*ignore-events* flag set.
This effectively clears all the input events, by flushing and ignoring them."
  (let ((cowl:*ignore-events* t))
    (glfw:poll-events)))


(defun teardown-input-callbacks ()
  "Removed all of the input callbacks. NB. this won't discriminate cowl-glfw callbacks
from anything else you might have put there."
  (glfw:set-mouse-button-callback nil)
  (glfw:set-mouse-pos-callback nil)
  (glfw:set-mouse-wheel-callback nil)
  (glfw:set-key-callback nil)
  (glfw:set-char-callback nil))

