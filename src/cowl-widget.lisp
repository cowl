(in-package #:cowl)

(compiler-opts)

;; (font *applications*)
(defclass widget-measurement ()
  ((current-offset :type real :initform 0
                   :initarg :current-offset :accessor current-offset-of
                   :documentation "The actual current offset relative to the parent widget")
   (current-size :type real :initform (and (default-font) (ftgl:get-font-line-height (default-font)))
                 :initarg :current-size :accessor current-size-of
                 :documentation "The current measurement size of the content (ie. excluding padding, border and margin).")
   (ideal-size :type real
               :initarg :ideal-size :accessor ideal-size-of
               :documentation "The ideal, preferred absolute size of the content of this measurement.")
   (constraints :type (cons real real)
                :initarg :constraints :accessor constraints-of
                :documentation "The (minimum . maximum) size for this measurement.")
   (margin :type (cons real real)
           :initarg :margin :initform (cons 2 2) :accessor margin-of
           :documentation "Cons-pair of margin widths in pixels outside the border of this widget.")
   (border-width :type (cons real real)
                 :initarg :border-width :initform (cons 1 1) :accessor border-width-of
                 :documentation "Cons-pair of widths in pixels of the border.")
   (padding :type (cons real real)
            :initarg :padding :initform (cons 2 2) :accessor padding-of
            :documentation "Cons-pair of widths in pixels of the border.")
   (border-colour :type (cons list list) :initarg :border-colour :accessor border-colour-of
                  :initform (cons '(1.0 1.0 1.0 1.0) '(1.0 1.0 1.0 1.0))
                  :documentation "Cons-pair of lists of RGBA float 0.0->1.0 values."))
  (:documentation "A measurement in one dimension (namely x or y) of an widget and
its associated data for laying out in containers."))

;; this warns because generic functions are implicly created 
;; perhaps use the (defgeneric :method syntax instead)?
(defmacro define-before-after-accessors (class &rest slots)
  `(progn
     ,@(mapcan
        #'(lambda (slot)
            `((defmethod ,(intern (format nil "BEFORE-~a-OF" slot)) ((,class ,class))
                (car (slot-value ,class ',slot)))
              (defmethod (setf ,(intern (format nil "BEFORE-~a-OF" slot))) (val (,class ,class))
                (setf (car (slot-value ,class ',slot)) val))
              (defmethod ,(intern (format nil "AFTER-~a-OF" slot)) ((,class ,class))
                (cdr (slot-value ,class ',slot)))
              (defmethod (setf ,(intern (format nil "AFTER-~a-OF" slot))) (val (,class ,class))
                (setf (cdr (slot-value ,class ',slot)) val))))
        slots)))

(define-before-after-accessors widget-measurement margin border-width padding border-colour)

(defmethod minimum-size-of ((widget-measurement widget-measurement))
  (car (constraints-of widget-measurement)))

(defmethod (setf minimum-size-of) (val (widget-measurement widget-measurement))
  (setf (car (constraints-of widget-measurement)) val))

(defmethod maximum-size-of ((widget-measurement widget-measurement))
  (cdr (constraints-of widget-measurement)))

(defmethod (setf maximum-size-of) (val (widget-measurement widget-measurement))
  (setf (cdr (constraints-of widget-measurement)) val))


(defmethod first-border-width-of ((widget-measurement widget-measurement))
  (with-slots (border-width) widget-measurement
    (if (consp border-width)
        (car border-width)
        border-width)))

(defmethod (setf total-current-size-of) (size (widget-measurement widget-measurement))
  "Computes the current internal size and sets it from an external total size."
  (setf (current-size-of widget-measurement)
        (- size
           (before-padding-of widget-measurement)
           (after-padding-of widget-measurement)
           (before-border-width-of widget-measurement)
           (after-border-width-of widget-measurement)
           (before-margin-of widget-measurement)
           (after-margin-of widget-measurement))))

(defmethod total-current-size-of ((widget-measurement widget-measurement))
  "Computes the total current outer size, including padding, border and margin of this dimension."
  (+ (slot-value widget-measurement 'current-size)
     (before-padding-of widget-measurement)
     (after-padding-of widget-measurement)
     (before-border-width-of widget-measurement)
     (after-border-width-of widget-measurement)
     (before-margin-of widget-measurement)
     (after-margin-of widget-measurement)))


(defmethod (setf total-ideal-size-of) (size (widget-measurement widget-measurement))
  "Computes the ideal internal size and sets it from an external total size."
  (setf (ideal-size-of widget-measurement)
        (- size
           (before-padding-of widget-measurement)
           (after-padding-of widget-measurement)
           (before-border-width-of widget-measurement)
           (after-border-width-of widget-measurement)
           (before-margin-of widget-measurement)
           (after-margin-of widget-measurement))))

(defmethod total-ideal-size-of ((widget-measurement widget-measurement))
  "Computes the total ideal outer size, including padding, border and margin of this dimension."
  (if (slot-boundp widget-measurement 'ideal-size)
      (+ (slot-value widget-measurement 'ideal-size)
         (before-padding-of widget-measurement)
         (after-padding-of widget-measurement)
         (before-border-width-of widget-measurement)
         (after-border-width-of widget-measurement)
         (before-margin-of widget-measurement)
         (after-margin-of widget-measurement))
      0))

(defmethod content-offset-of ((widget-measurement widget-measurement))
  "Offset from the top left of the widget to the content start"
  (+ (before-margin-of widget-measurement)
     (before-border-width-of widget-measurement)
     (before-padding-of widget-measurement)))

;; this references cowl contaner before it is declared, ok, but leads to warnings
(defclass widget ()
  ((parent :accessor parent-of :type widget :initarg :parent)
   (visible :accessor visibility-of :type boolean
            :initform t :initarg :visible
            :documentation "Inhibits the widget from being drawn. It is still
present in the layout though.")
   (x :accessor x :type widget-measurement
      :initform (make-instance 'widget-measurement))
   (y :accessor y :type widget-measurement
      :initform (make-instance 'widget-measurement))
   (foreground-colour :type list
                      :accessor foreground-colour-of
                      :documentation "Colour of the text, default solid border
colour and highlight.")
   (background-colour :type list
                      :accessor background-colour-of
                      :documentation "Colour of the background of the widget
and base colour of default inset/outset border."))
  (:documentation "The base class for all on-screen-display widgets"))

(defmethod initialize-instance :after
    ((widget widget) &key x y width height style padding margin
     padding-left padding-right padding-top padding-bottom
     margin-left margin-right margin-top margin-bottom
     border-width-left border-width-right border-width-top border-width-bottom
     foreground-colour background-colour
     (border-style :solid border-stylep)
     (border-width 1 border-widthp)
     (border-colour nil border-colourp)
     &allow-other-keys)
  (when (realp x) (setf (current-offset-of (x widget)) x))
  (when (realp y) (setf (current-offset-of (y widget)) y))
  (when (realp width)  (setf (ideal-size-of (x widget))   width
                             (current-size-of (x widget)) width))
  (when (realp height) (setf (ideal-size-of (y widget))   height
                             (current-size-of (y widget)) height))

  ;; style settings -  borders depend on colours.
  (setf (foreground-colour-of widget)
        (or foreground-colour (get-style-value widget :foreground-colour style))
        (background-colour-of widget)
        (or background-colour (get-style-value widget :background-colour style))
        (margin-of widget)
        (or margin (get-style-value widget :margin style))
        (padding-of widget)
        (or padding (get-style-value widget :padding style)))

  ;; pedantic paddings and margins widths
  (when padding-left (setf (before-padding-of (x widget)) padding-left))
  (when padding-right (setf (after-padding-of (x widget)) padding-right))
  (when padding-top (setf (before-padding-of (y widget)) padding-top))
  (when padding-bottom (setf (after-padding-of (y widget)) padding-bottom))
  (when margin-left (setf (before-margin-of (x widget)) margin-left))
  (when margin-right (setf (after-margin-of (x widget)) margin-right))
  (when margin-top (setf (before-margin-of (y widget)) margin-top))
  (when margin-bottom (setf (after-margin-of (y widget)) margin-bottom))

  (if (or border-stylep border-widthp border-colourp)
      (set-borders widget border-style border-width border-colour)
      (let ((borders (get-style-value widget :borders style)))
        (when borders
          (apply #'set-borders (cons widget borders)))))

  ;; pedantic borders widths
  (when border-width-left (setf (before-border-width-of (x widget)) border-width-left))
  (when border-width-right (setf (after-border-width-of (x widget)) border-width-right))
  (when border-width-top (setf (before-border-width-of (y widget)) border-width-top))
  (when border-width-bottom (setf (after-border-width-of (y widget)) border-width-bottom)))

(defun set-widget-style (widget style)
  (declare (type widget widget))

  ;; style settings -  borders depend on colours.
  (setf (foreground-colour-of widget)
        (get-style-value widget :foreground-colour style)
        (background-colour-of widget)
        (get-style-value widget :background-colour style)
        (margin-of widget)
        (get-style-value widget :margin style)
        (padding-of widget)
        (get-style-value widget :padding style))
  (let ((borders (get-style-value widget :borders style)))
    (when borders
      (apply #'set-borders (cons widget borders)))))


(defmethod detach ((widget widget))
  "Callback for when an widget is detached from the current view (ie. it goes off the screen).")

(defmethod (setf padding-of) (val (widget widget))
  (setf (padding-of (x widget)) (cons val val)
        (padding-of (y widget)) (cons val val)))

(defmethod (setf margin-of) (val (widget widget))
  (setf (margin-of (x widget)) (cons val val)
        (margin-of (y widget)) (cons val val)))

(defmethod ideal-width-of ((widget widget))
  (if (slot-boundp (x widget) 'ideal-size)
      (ideal-size-of (x widget))
      0))

(defmethod ideal-height-of ((widget widget))
  (if (slot-boundp (y widget) 'ideal-size)
      (ideal-size-of (y widget))
      0))

(defmethod total-ideal-width-of ((widget widget))
  (with-accessors ((widget-measurement x)) widget
    (+ (ideal-width-of widget)
       (before-padding-of widget-measurement)
       (after-padding-of widget-measurement)
       (before-border-width-of widget-measurement)
       (after-border-width-of widget-measurement)
       (before-margin-of widget-measurement)
       (after-margin-of widget-measurement))))

(defmethod total-ideal-height-of ((widget widget))
  (with-accessors ((widget-measurement y)) widget
    (+ (ideal-height-of widget)
       (before-padding-of widget-measurement)
       (after-padding-of widget-measurement)
       (before-border-width-of widget-measurement)
       (after-border-width-of widget-measurement)
       (before-margin-of widget-measurement)
       (after-margin-of widget-measurement))))

(defmethod cumulative-offset-of ((widget widget))
  "Returns the cumulative offset in the viewport as (list x y)"
  (apply #'mapcar (cons #'+ (loop for n = widget then (and (slot-boundp n 'parent) (parent-of n))
                               while n
                               nconcing
                               (list (list (current-offset-of (x n)) (current-offset-of (y n)))
                                     (list (before-margin-of (x n)) (before-margin-of (y n)))
                                     (list (before-border-width-of (x n)) (before-border-width-of (y n)))
                                     (list (before-padding-of (x n)) (before-padding-of (y n))))))))

(defmethod current-offset-before-border-of ((widget widget))
  (destructuring-bind (cx cy) (cumulative-offset-of widget)
    (list (+ cx (before-border-width-of (x widget)))
          (+ cy (before-border-width-of (y widget))))))

(defmethod current-offset-before-border-of ((widget widget))
  (list (+ (current-offset-of (x widget))
           (before-border-width-of (x widget)))
        (+ (current-offset-of (y widget))
           (before-border-width-of (y widget)))))

(defmethod set-borders ((widget widget) style &optional (width 1) colour)
  (declare (type (member :solid :inset :outset :none) style))
  "Set up borders to one of the four presets."
  (with-slots (x y) widget
    (flet ((dark-light (delta)
             (let ((base-colour (or colour
                                    (if (slot-boundp widget 'background-colour)
                                        (background-colour-of widget)
                                        '(0.5 0.5 0.5 0.5)))))
               (cons (list (- (elt base-colour 0) delta)
                           (- (elt base-colour 1) delta)
                           (- (elt base-colour 2) delta)
                           (elt base-colour 3))
                     (list (+ (elt base-colour 0) delta)
                           (+ (elt base-colour 1) delta)
                           (+ (elt base-colour 2) delta)
                           (elt base-colour 3))))))
      (ecase style
        (:solid
         (unless colour (setf colour
                              (if (slot-boundp widget 'foreground-colour)
                                  (foreground-colour-of widget)
                                  '(1.0 1.0 1.0 1.0))))
         (setf (border-width-of x) (cons width width)
               (border-width-of y) (cons width width)
               (border-colour-of x) (cons colour colour)
               (border-colour-of y) (cons colour colour)))
        (:outset
         (setf (border-width-of x) (cons width width)
               (border-width-of y) (cons width width)
               (border-colour-of x) (dark-light -0.25)
               (border-colour-of y) (dark-light -0.25)))
        (:inset
         (setf (border-width-of x) (cons width width)
               (border-width-of y) (cons width width)
               (border-colour-of x) (dark-light 0.25)
               (border-colour-of y) (dark-light 0.25)))
        ((nil :none)
         (setf (border-width-of x) (cons 0 0)
               (border-width-of y) (cons 0 0)))))))

(defmethod layout ((widget widget))
  "Lay-out the contents of the widget when the sizes/offsets change.")


(defmacro with-widget-box-bindings (widget &body forms)
  "Makes bindings for b[xy][01][01] thus:
      bx00  bx01                   bx10 bx11
 by00 +     +----------------------+    +
            |                      |
 by01 +-----+----------------------+----+
      |     |                      |    |
      |     |                      |    |
      |     |      Content         |    |
      |     |                      |    |
      |     |                      |    |
 by10 +-----+----------------------+----+
            |                      |
 by11 +     +----------------------+    "
  (let ((widget-s (gensym "WIDGET-"))
        (x (gensym "X-"))
        (y (gensym "Y-")))
    `(let* ((,widget-s ,widget)
            (,x (x ,widget-s))
            (,y (y ,widget-s))
            (bx00 (before-margin-of ,x))
            (bx01 (+ bx00 (before-border-width-of ,x)))
            (bx10 (+ bx01 (current-size-of ,x) (before-padding-of ,x) (after-padding-of ,x)))
            (bx11 (+ bx10 (after-border-width-of ,x)))
            (by00 (before-margin-of ,y))
            (by01 (+ by00 (before-border-width-of ,y)))
            (by10 (+ by01 (current-size-of ,y) (before-padding-of ,y) (after-padding-of ,y)))
            (by11 (+ by10 (after-border-width-of ,y))))
       ,@forms)))


(defmethod draw ((widget widget))
  "No-op method here for testing")

(defmethod draw :around ((widget widget))
  "Draws the borders and background colour, if approcpriate, of the widget and translates it to the right position for drawing.
This is the border layout.
      bx00  bx01                   bx10 bx11
 by00 +     +----------------------+    +
            |                      |
 by01 +-----+----------------------+----+
      |     |                      |    |
      |     |                      |    |
      |     |      Content         |    |
      |     |                      |    |
      |     |                      |    |
 by10 +-----+----------------------+----+
            |                      |
 by11 +     +----------------------+    +"

  (unless (visibility-of widget)
    (return-from draw))

  (with-slots (x y) widget
    (gl:disable gl:+texture-2d+)

    (with-widget-box-bindings widget
      ;; left border
      (when (plusp (before-border-width-of x))
        (apply #'gl:color-4f (before-border-colour-of x))
        (gl:rect-f bx00 by10 bx01 by01))
      ;; top border
      (when (plusp (before-border-width-of y))
        (apply #'gl:color-4f (before-border-colour-of y))
        (gl:rect-f bx01 by01 bx10 by00))
      ;; right border
      (when (plusp (after-border-width-of x))
        (apply #'gl:color-4f (after-border-colour-of x))
        (gl:rect-f bx10 by10 bx11 by01))
      ;; bottom border
      (when (plusp (after-border-width-of y))
        (apply #'gl:color-4f (after-border-colour-of y))
        (gl:rect-f bx01 by11 bx10 by10))

      ;; background
      (when (slot-boundp widget 'background-colour)
        (apply #'gl:color-4f (background-colour-of widget))
        (gl:rect-f bx10 by01 bx01 by10))

      (when (next-method-p)
        (gl:with-push-matrix
          (gl:translate-f (coerce (+ bx01 (before-padding-of x)) 'single-float)
                          (coerce (+ by01 (before-padding-of y)) 'single-float)
                          0.0)
          (call-next-method))))))