(in-package #:cowl)

(defclass file-selector (container)
  ((directory-input :type input :accessor directory-input-of)
   (file-input :type input :accessor file-input-of)
   (files :type simple-vector :accessor files-of)
   (file-list :type container :accessor file-list-of)
   (confirm-callback :type function :initarg confirm-callback :accessor confirm-callback-of)
   (cancel-callback :type function :initarg cancel-callback :accessor cancel-callback-of)
   (valid-selection-p :type function :initarg :valid-selection-p :accessor valid-selection-p-of)
   (valid-directory-p :type function :initarg :valid-directory-p :initform #'(lambda (path) (not (pathname-name path)))
                      :accessor valid-directory-p-of)))


(defun file-selector-update-listing (file-selector)
  (declare (type file-selector file-selector))
  (with-accessors ((files files-of)) file-selector
    (setf files (make-array 1 :initial-contents
                            (directory (make-pathname :name :wild :type :wild :version :wild 
                                                      :defaults (parse-namestring (text-of (directory-input-of file-selector)))))))
    ;; TODO split this up to render elsewhere, paying attention to scroll bar and its shuttle size.
    (let ((entries files))
      (dotimes (bi (array-dimension (contents-of (file-list-of file-selector)) 0))
        (let ((entry (pop entries))) 
          (format t "entry in directory ~s => ~s~%"  (make-pathname :name :wild :type :wild :version :wild 
                                                                    :defaults (parse-namestring (text-of (directory-input-of file-selector)))) entry)
          (let ((element (aref (contents-of (file-list-of file-selector)) bi 0)))
            (if entry
                (setf (visibility-of element) t
                      (text-of element)
                      (if (pathname-name entry)
                          (file-namestring entry)
                          (directory-namestring (make-pathname :directory (list :relative (car (last (pathname-directory entry))))))))
                (setf (visibility-of element) nil
                      (text-of element) ""))))))))
    

(defun file-selector-pathname (file-selector)
  (declare (type file-selector file-selector))
  (merge-pathnames 
   (parse-namestring (text-of (file-input-of file-selector)))
   (parse-namestring (text-of (directory-input-of file-selector)))))


(defmethod initialize-instance :after ((file-selector file-selector) &key (default-path *default-pathname-defaults*) (title "Select file") (confirm-button-text "Select") (rows 10))
  (declare (optimize (debug 3)))
  (with-accessors ((directory-input directory-input-of)
                   (file-input file-input-of)
                   (file-list file-list-of)
                   (contents contents-of)
                   (row-heights row-heights-of)
                   (column-widths column-widths-of)) file-selector
    (let ((2lh (* 2 (ftgl:get-font-line-height *font*)))
          (1.5lh (* 1.5 (ftgl:get-font-line-height *font*))))
      (setf
       directory-input
       (make-instance 'cowl:input :text (directory-namestring default-path)
                      :writer #'(lambda (text)
                                  (declare (ignore text))
                                  (file-selector-update-listing file-selector)))
       file-input
       (make-instance 'cowl:input :text (file-namestring default-path)
                      :writer #'(lambda (text)
                                  (let ((path (file-selector-pathname file-selector)))
                                    (setf (text-of directory-input) (directory-namestring path)
                                          (text-of file-input) (file-namestring path))
                                    (file-selector-update-listing file-selector))))
       file-list
       (make-instance 'container
                      :contents (loop repeat rows collect (list (make-button () ""
                                                                  (when (and (text-of self) (plusp (length (text-of self))))
                                                                    (setf (text-of file-input) (text-of self))
                                                                    (funcall (writer-of file-input) (text-of self))))))
                      :row-heights (make-list rows :initial-element `(:expand . ,1.5lh))
                      :style :vbox)
       contents
       (list 
        (list (make-label title) nil)
        (list (make-hbox ()
                (make-label "Directory: ")
                (make-button () "Up"
                  (setf (text-of directory-input) 
                        (let ((dp (parse-namestring (text-of directory-input))))
                          (directory-namestring (make-pathname :directory (butlast (pathname-directory dp)) :defaults dp))))
                  (file-selector-update-listing file-selector))
                directory-input)
              nil)
        (list file-list
              (make-instance 'cowl:slider :height (* rows 1.5lh)))
        (list 
         (make-hbox ()
           (make-label "File: ")
           file-input
           (make-button () confirm-button-text
             (when (or (not (slot-boundp file-selector 'valid-selection-p))
                       (funcall (valid-selection-p-of file-selector)))
               (when (slot-boundp file-selector 'confirm-callback)
                 (funcall (confirm-callback-of file-selector)))))
           (make-button () "Cancel"
             (when (slot-boundp file-selector 'cancel-callback)
               (funcall (cancel-callback-of file-selector)))))
         nil))
       row-heights
       `((:expand . ,2lh) (:weight . 1.0) (:expand . ,2lh) (:expand . ,2lh)))
      (file-selector-update-listing file-selector))))
