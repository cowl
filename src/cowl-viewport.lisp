
(in-package #:cowl)

(defclass viewport (widget)
  ((render :type function :initarg :render :accessor render-of
           :documentation "Callback for this class for drawing the scene inside the viewport."))
  (:documentation "Widget that contains a miniature viewport,
use the render attribute, or draw-viewport-scene method for derived classes."))

(defgeneric draw-viewport-scene (viewport)
  (:documentation "Override this method in sub-classes of viewport to
define the render function of the viewport."))

(defmethod draw ((viewport viewport))
  (destructuring-bind (display-width display-height)
      (let ((viewport (list 0 0 0 0)))
        (gl:get-integerv gl:+viewport+ viewport)
        (cddr viewport))
    (gl:with-push-attrib (gl:+depth-buffer-bit+)
      (gl:enable gl:+depth-test+)
      (gl:depth-func gl:+always+)
      (gl:color-4f 0.5 0.5 0.5 0)
      (gl:translate-f 0 0 -1)
      (gl:rect-f 0 (current-size-of (y viewport)) 
                 (current-size-of (x viewport)) 0))
    (destructuring-bind (x y) (cumulative-offset-of viewport)
      (gl:with-push-attrib (gl:+viewport-bit+)
        (let ((height (round (current-size-of (y viewport)))))
          (gl:viewport (round x) 
                       (- display-height (round y) height) 
                       (round (current-size-of (x viewport)))
                       height))
        (draw-viewport-scene viewport)))))

(defmethod draw-viewport-scene ((viewport viewport))
  (funcall (render-of viewport)))

(defmacro make-viewport ((&rest make-instance-keyargs) &body render-forms)
  "Convenience macro that takes the keywords and a render function body."
  `(make-instance 'viewport ,@make-instance-keyargs
                  :render #'(lambda () ,@render-forms)))

;; cityscape specific 
;; (defclass model-viewer (viewport)
;;   ((model-uri :type model-uri :initarg :model-uri :initform nil :accessor model-uri-of
;;               :documentation "Unique identifier of the model to be drawn.")
;;    (x-rotation :type single-float :initform -45.0 :initarg :x-rotation :accessor x-rotation-of
;;                :documentation "Angle from straight-down this model is rotated by.")
;;    (z-rotation :type single-float :initform 0.0 :initarg :z-rotation :accessor z-rotation-of
;;                :documentation "Current state of the z-rotation."))
;;   (:documentation "Viewport widget that renders a centred, rotating model."))

;; (defmethod draw-viewport-scene ((model-viewer model-viewer))
;;   (with-slots (model-uri distance x-rotation z-rotation) model-viewer
;;     (when model-uri
;;       (let* ((width (current-size-of (x model-viewer)))
;;              (height (current-size-of (y model-viewer)))
;;              (model (get-model model-uri))
;;              (distance (/ (maths:modulus (mapcar #'-
;;                                                  (first (extents-of model)) 
;;                                                  (second (extents-of model))))
;;                           (tan (/ *fov* 2)))))
;;         (setf z-rotation (* (glfw:get-time) 10.0))
;;         (gl:with-projection-matrix 
;;             ((glu:perspective *fov*
;;                               (coerce (/ width height) 'single-float)
;;                               (coerce (/ distance 16) 'single-float)
;;                               (coerce (* distance 2) 'single-float)))
;;           (gl:load-identity)
;;           (gl:with-push-attrib (gl:+texture-bit+)
;;             (gl:disable gl:+texture-2d+)
;;             (gl:translate-f 0 0 (- distance))
;;             (gl:rotate-f x-rotation 1 0 0)
;;             (gl:rotate-f z-rotation 0 0 1)
;;             ;; translate down by half of the model's z extents
;;             (gl:translate-f 0 0 (/ (apply #'+ (mapcar #'third (extents-of model)))
;;                                    -2))
;;             (gl:color-4f 1 1 1 1)
;;             (draw model)))))))

