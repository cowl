(in-package #:cowl)
(compiler-opts)

(defclass container (widget)
  ((contents :type (simple-array (or null widget) 2) :reader contents-of
             :documentation "A matrix of contained widgets")
   (row-heights :type list :accessor row-heights :initarg :row-heights :accessor row-heights-of
                  :documentation "Heights of the rows, list of either a stretch-weight (:weight . number), 
or a fixed value (:fixed . number).")
   (column-widths :type list :accessor column-widths :initarg :column-widths :accessor column-widths-of
                  :documentation "Widths of the columns, list of either a stretch-weight (:weight . number), 
or a fixed value (:fixed . number).")
   (computed-row-heights :type list :accessor computed-row-heights-of)
   (computed-column-widths :type list :accessor computed-column-widths-of))
  (:documentation "A grid-layout container for other widgets."))

(defmethod (setf contents-of) (ncontents (container container))
  (with-slots (contents) container
    (cond 
      ((listp ncontents)
       (unless (listp (first ncontents)) (error "contents must be a list of lists"))
       ;; (format t "Making a ~ax~a grid~%" (length contents) (length (first contents)))
       (setf contents
             (make-array (list (length ncontents) (length (first ncontents)))
                         :element-type '(or null widget)
                         :initial-contents ncontents)))
      ((arrayp ncontents)
       (setf contents ncontents))))
  (with-accessors ((contents contents-of)) container
    (loop for row below (array-dimension contents 0) do
         (loop for column below (array-dimension contents 1) do
              (let ((widget (aref contents row column)))
                (when widget
                  (setf (parent-of widget) container)))))
    (unless (slot-boundp container 'row-heights)
      (setf (slot-value container 'row-heights)
            (make-list (array-dimension contents 0)
                       :initial-element (cons :expand (if (default-font)
                                                          (ftgl:get-font-line-height (default-font))
                                                          10))))
      ;; (format t "row heights unintialized, set to ~a~%" (slot-value container 'row-heights))
      )
    (unless (slot-boundp container 'column-widths)
      (setf (slot-value container 'column-widths)
            (make-list (array-dimension contents 1) 
                       :initial-element (cons :expand (if (default-font)
                                                          (ftgl:get-font-line-height (default-font))
                                                          10))))
      ;; (format t "column widths unintialized, set to ~a~%" (slot-value container 'column-widths))
      )))


(defmethod initialize-instance :after ((container container) &key rows columns contents)
  (cond 
    (contents
     (setf (contents-of container) contents))
    ((and (realp columns) (realp rows))
     (setf (contents-of container) 
           (make-array (list rows columns)
                       :element-type '(or null widget)
                       :initial-element nil)))))

(defmethod set-cell ((container container) row column widget)
  (when (aref (contents-of container) row column)
    (detach (aref (contents-of container) row column)))
  (setf (aref (contents-of container) row column)
        widget)
  (when widget 
    (setf (parent-of widget) container)))

(defun row-widget-heights-of (2d-array)
  (loop for row below (array-dimension 2d-array 0) collecting
       (loop for column below (array-dimension 2d-array 1) collecting
            (let ((widget (aref 2d-array row column)))
              (if widget
                  (total-ideal-height-of widget)
                  0)))))

(defun column-widget-widths-of (2d-array)
  (loop for column below (array-dimension 2d-array 1) collecting
       (loop for row below (array-dimension 2d-array 0) collecting
            (let ((widget (aref 2d-array row column)))
              (if widget
                  (total-ideal-width-of widget)
                  0)))))

(defmethod contents-list ((container container))
  "Just collect up all of the contents into one list, ignoring empty cells."
  (loop for column below (array-dimension (contents-of container) 1) nconcing
       (loop for row below (array-dimension (contents-of container) 0)
            when (aref (contents-of container) row column)
          collecting
            (aref (contents-of container) row column))))

(defun compute-ideal-sizes (sizes set-ideal-sizes)
  (loop for size in sizes
     for ideal-sizes in set-ideal-sizes
     collecting
     (ecase (car size)
       ((:expand :weight) (reduce #'max (cons (cdr size) ideal-sizes)))
       (:fixed (cdr size)))))


(defmethod ideal-width-of ((container container))
  (if (slot-boundp (x container) 'ideal-size)
      (slot-value (x container) 'ideal-size)
      (reduce #'+
              (compute-ideal-sizes (column-widths-of container)
                                   (column-widget-widths-of (contents-of container))))))

(defmethod ideal-height-of ((container container))
  (if (slot-boundp (y container) 'ideal-size)
      (slot-value (y container) 'ideal-size)
      (reduce #'+
              (compute-ideal-sizes (row-heights-of container)
                                   (row-widget-heights-of (contents-of container))))))


(defun compute-set-sizes (available-size sizes set-ideal-sizes)
  (let* ((available-weighted-size available-size)
         (total-weight 0.0)
         (out-sizes 
          (loop for size in sizes
             for ideal-sizes in set-ideal-sizes
             collecting
               (ecase (car size)
                 (:weight 
                  (incf total-weight (cdr size))
                  size)
                 ((:expand :fixed)
                  (let ((size (min available-weighted-size
                                   (ecase (car size)
                                     (:expand (reduce #'max (cons (cdr size) ideal-sizes)))
                                     (:fixed (cdr size))))))
                    (decf available-weighted-size size)
                    size))))))
    (map-into out-sizes
              #'(lambda (size)
                  (if (consp size)
                      (* (cdr size) (/ available-weighted-size total-weight))
                      size))
              out-sizes)))

(defmethod detach ((container container))
  "Callback for when an widget is detached from the current view (ie. it goes off the screen)."
  (mapcar #'detach (contents-list container)))

(defmethod layout ((container container))
  (setf (computed-row-heights-of container) 
        (compute-set-sizes (current-size-of (y container))
                           (row-heights-of container)
                           (row-widget-heights-of (contents-of container)))
        (computed-column-widths-of container) 
        (compute-set-sizes (current-size-of (x container))
                           (column-widths-of container)
                           (column-widget-widths-of (contents-of container))))
  ;; (format t "computed ~a~%  widths~a~%  heights~a~%" 
  ;; 	  container 
  ;; 	  (computed-column-widths-of container)
  ;; 	  (computed-row-heights-of container))
  (let ((ix 0.0) (iy 0.0))
    (loop for row below (array-dimension (contents-of container) 0) 
       for height in (computed-row-heights-of container) do
       (loop for column below (array-dimension (contents-of container) 1) 
          for width in (computed-column-widths-of container) do
	  (let ((widget (aref (contents-of container) row column)))
	    (when widget
	      (setf (total-current-size-of (x widget)) (min width (total-ideal-width-of widget))
		    (total-current-size-of (y widget)) (min height (total-ideal-height-of widget))
		    (current-offset-of (x widget)) ix
		    (current-offset-of (y widget)) iy)
	      (layout widget)))
          (incf ix width))
       (incf iy height)
       (setf ix 0.0))))

(defmethod draw ((container container))
  (layout container)
  (with-slots (x y) container
    (mapcar #'(lambda (widget)
                (gl:with-push-matrix
                  (gl:translate-f (current-offset-of (x widget)) 
                                  (current-offset-of (y widget))
                                  0.0)
                  (draw widget)))
            (contents-list container))))

(defmethod update-text ((container container))
  (mapc #'update-text (contents-list container)))


(defmacro define-propogating-container-method (name (&rest lambda-list) &body forms)
  "Defines a method named name, with lambda-list prepended by (container container),
that propogates to child widgets, x and y must appear in the lambda list.
For the course of forms, widget is also bound to the widget located."
  `(defmethod ,name ,(list* '(container container) lambda-list)
     (declare (optimize (debug 3)))
     (when (and (slot-boundp container 'computed-row-heights)
                (slot-boundp container 'computed-column-widths))
       (let ((y-row 0))
         (loop for row-height in (computed-row-heights-of container)
            for iy from 0 do
            (let ((x-column 0))
              (loop for column-width in (computed-column-widths-of container)
                 for ix from 0 do
                 (when (and (>= x x-column)
                            (<= x (+ column-width x-column))
                            (>= y y-row)
                            (<= y (+ row-height y-row)))
                   (let ((widget (aref (contents-of container) iy ix)))
                     (when widget
                       ,@forms)))
                 (incf x-column column-width)))
            (incf y-row row-height))))))

(define-propogating-container-method handle-mouse-button (button press x y)
  (handle-mouse-button widget
                       button press
                       (- x (current-offset-of (x widget)))
                       (- y (current-offset-of (y widget)))))

(define-propogating-container-method handle-mouse-wheel (zrel x y)
  (handle-mouse-wheel widget
                      zrel
                      (- x (current-offset-of (x widget)))
                      (- y (current-offset-of (y widget)))))




(defmacro contents-iterate-forwards (contents widget-name &body forms)
  (let ((x (gensym "X"))
	(y (gensym "Y"))
	(c (gensym "CONTENTS")))
    `(let ((,c ,contents))
       (loop for ,y below (array-dimension ,c 0) do
	    (loop for ,x below (array-dimension ,c 1) do
		 (let ((,widget-name (aref ,c ,y ,x)))
		   ,@forms))))))

(defmacro contents-iterate-backwards (contents widget-name &body forms)
  (let ((x (gensym "X"))
	(y (gensym "Y"))
	(c (gensym "CONTENTS")))
    `(let ((,c ,contents))
       (loop for ,y from (1- (array-dimension ,c 0)) downto 0 do
	    (loop for ,x from (1- (array-dimension ,c 1)) downto 0 do
		 (let ((,widget-name (aref ,c ,y ,x)))
		   ,@forms))))))


(defun find-first-focusable (widget)
  "Find the first focusable widget that is widget, or is in widget.
Depth-first search"
  (declare (type widget widget))
  (cond
    ((accept-focus-p widget)
     widget)
    ((typep widget 'container)
     (contents-iterate-forwards (contents-of widget) w
       (when (and w (setf w (find-first-focusable w)))
	 (return-from find-first-focusable w))))))

(defun find-last-focusable (widget)
  "Find the last focusable widget that is widget, or is in widget.
Depth-first search"
  (declare (type widget widget))
  (cond
    ((accept-focus-p widget)
     widget)
    ((typep widget 'container)
     (contents-iterate-backwards (contents-of widget) w
       (when (and w (setf w (find-last-focusable w)))
	 (return-from find-last-focusable w))))))


(defun next-widget (widget)
  (declare (type widget widget))
  (let ((parent (when (slot-boundp widget 'parent)
		  (parent-of widget))))
    (when parent
      (when (typep parent 'container)
	(let (found)
	  (contents-iterate-forwards (contents-of parent) w
	    (if found
		(when (and w (setf w (find-first-focusable w)))
		  (return-from next-widget w))
		;;Found our own widget in the container, from now on, search for something focusable
		(when (eql w widget)
		  (setf found t))))))
      ;;Nothing was found - go up a level
      (next-widget parent))))

(defun prev-widget (widget)
  (declare (type widget widget))
  (let ((parent (when (slot-boundp widget 'parent)
		  (parent-of widget))))
    (when parent
      (when (typep parent 'container)
	(let (found)
	  (contents-iterate-backwards (contents-of parent) w
	    (if found
		(when (and w (setf w (find-last-focusable w)))
		  (return-from prev-widget w))
		;;Found our own widget in the container, from now on, search for something focusable
		(when (eql w widget)
		  (setf found t))))))
      ;;Nothing was found - go up a level
      (prev-widget parent))))