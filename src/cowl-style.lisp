(in-package #:cowl)

(defparameter *colour-transparent* '(0.0 0.0 0.0 0.0))
(defparameter *colour-white* '(1.0 1.0 1.0 1.0))
(defparameter *colour-black* '(0.0 0.0 0.0 1.0))

(defparameter *button-background-colour* '(0.9 0.8 0.7 0.8))
(defparameter *button-foreground-colour* *colour-black*)

(defparameter *button-depressed-background-colour* '(0.8 0.7 0.6 0.8))

(defparameter *input-background-colour*         '(0.75 0.75 0.75 0.8))
(defparameter *red-input-background-colour*     '(1.0  0.75 0.75 0.8))
(defparameter *green-input-background-colour*   '(0.75 1.0  0.75 0.8))
(defparameter *blue-input-background-colour*    '(0.75 0.75 1.0  0.8))
(defparameter *yellow-input-background-colour*  '(1.0  1.0  0.75 0.8))
(defparameter *magenta-input-background-colour* '(1.0  0.75 1.0  0.8))
(defparameter *cyan-input-background-colour*    '(0.75 1.0  1.0  0.8))

(defparameter *generic-background-colour* '(0.1 0.0 0.2 0.75))
(defparameter *generic-foreground-colour* '(1.0 0.9 0.8 1.0))

(defparameter *translucent-generic-background-colour* '(0.1 0.0 0.2 0.375))

(defun get-generic-style (widget)
  (declare (type widget widget))
  (cond
    ;; most specific classes must come first
    ((typep widget 'input) :input)
    ((typep widget 'button) :button)
    ((typep widget 'text) :text)
    ((typep widget 'slider) :slider)
    ((typep widget 'container) :container)))


(defparameter *stylesheet*
  (list :default
        (list :background-colour *generic-background-colour*
              :foreground-colour *generic-foreground-colour*
              :padding 0
              :margin 1
              :borders (list :solid 1 *generic-foreground-colour*))
        :button
        (list :background-colour *button-background-colour*
              :foreground-colour *button-foreground-colour*
              :padding 1
              :borders '(:outset))
        :input
        (list :background-colour *input-background-colour*
              :foreground-colour *button-foreground-colour*
              :padding 1
              :borders '(:inset))
        :depressed-button
        (list :background-colour *button-depressed-background-colour*
              :foreground-colour *button-foreground-colour*
              :padding 1
              :borders '(:inset))
        :slider
        (list :background-colour *translucent-generic-background-colour*
              :padding 1
              :borders (list :inset 1 *generic-background-colour*))
        :shuttle
        (list :background-colour *button-background-colour*
              :background-colour *button-foreground-colour*
              :padding 0
              :margin 0
              :borders '(:outset))
        '(:hbox :vbox :grid :container)
        (list :padding 0
              :margin 0
              :background-colour *colour-transparent*
              :foreground-colour *colour-white*
              :borders '(:none))
        '(:label :text-block :text)
        (list :background-colour *generic-background-colour*
              :foreground-colour *generic-foreground-colour*
              :padding 1
              :borders (list :solid 1 *translucent-generic-background-colour*))))



(defun get-style-value (widget property &optional style)
  (declare (type widget widget))
  (unless style
    (setf style (get-generic-style widget)))
  (or (loop for name in *stylesheet* by #'cddr
         for properties in (cdr *stylesheet*) by #'cddr
         when
         (or (eql style name)
             (and (listp name)
                  (find style name)))
         return
         (getf properties property))
      (if (eql style :default)
          (error "No property ~S set for :default in *stylesheet*." property)
          (get-style-value widget property :default))))


