(in-package #:cowl)

(compiler-opts)

(defclass button (widget)
  ((left-click   :type (function (widget) boolean) :initarg :left-click   :accessor left-click-of
                 :documentation "Function to be called on a left click of the mouse button.")
   (middle-click :type (function (widget) boolean) :initarg :middle-click :accessor middle-click-of
                 :documentation "Function to be called on a middle click of the mouse button.")
   (right-click  :type (function (widget) boolean) :initarg :right-click  :accessor right-click-of
                 :documentation "Function to be called on a right click of the mouse button.")
   (wheel :type (function (widget integer) boolean) :initarg :wheel :accessor wheel-of
                 :documentation "Function to be called when the mouse wheel is rolled over the widget."))
  (:documentation "A clickable OSD GUI button. All of the slots should signal the event-handled condition
when this event has been accounted for completely. Otherwise it may propogate further down the tree
or trigger other event handlers."))

(defclass text-button (button text) ()
  (:documentation "A class mixing text and button."))

(defmethod initialize-instance :after ((text-button text-button) &key (wrap nil))
  (setf (wrap text-button) wrap))

(defmethod handle-mouse-button ((widget button) button press x y)
  (declare (type (member :left :middle :right) button))
  (declare (type boolean press))
  (when press
    (with-widget-box-bindings widget
      (when (and (>= x bx00) (<= x bx11)
                 (>= y by00) (<= y by11))
        (case button
          (:left
           (when (slot-boundp widget 'left-click)
             (funcall (left-click-of widget) widget)))
          (:middle
           (when (slot-boundp widget 'middle-click)
             (funcall (middle-click-of widget) widget)))
          (:right
           (when (slot-boundp widget 'right-click)
             (funcall (right-click-of widget) widget))))))))

(defmethod handle-mouse-wheel ((button button) zrel x y)
  (with-widget-box-bindings button
    (when (and (slot-boundp button 'wheel)
               (>= x bx00) (<= x bx11)
               (>= y by00) (<= y by11))
      (funcall (wheel-of button) button zrel))))