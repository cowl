
(defpackage #:cowl
  (:use #:cffi #:cl #:cl-glfw-opengl)
  (:shadowing-import-from #:cl #:boolean #:byte #:float #:char #:string)
  (:export 
   ;; Classes
   #:widget #:viewport #:text #:slider #:progress #:numeric-input
   #:graph #:container #:button #:input #:password-input #:file-selector

   ;; Parameters
   #:*root-widget*

   ;; callback hooks
   #:*key-handlers*
   #:*mouse-button-handlers*
   #:*mouse-motion-handlers*
   #:*mouse-wheel-handlers*

   ;; Default font manipluation
   #:*font*
   #:*default-font-size*
   #:default-font
   #:delete-default-font

   ;; event injection
   #:key-event
   #:mouse-button-event
   #:mouse-motion-event
   #:mouse-wheel-event

   ;; event status
   #:*focused-widget*
   #:*ignore-events*
   #:*held-keys*
   #:*prev-mouse*

   ;; widget methods
   #:blur
   #:focus
   #:layout-root
   #:layout
   #:draw-root
   #:draw
   #:foreground-colour-of
   #:background-colour-of
   #:x
   #:y
   #:visibility-of
   #:parent-of

   ;; context specific handles
   #:self
   
   ;; container widget
   #:set-cell
   #:contents-list
   
   ;; button widget
   #:left-click-of
   #:middle-click-of
   #:right-click-of
   #:wheel-of

   ;; graph widget
   #:graph-data-of

   ;; text widget
   #:text-of
   #:display-text-of
   #:update-text
   #:text-updater-of
   #:font-of

   ;; input widget
   #:accept-char-p
   #:*enter-edit-mode-hooks*
   #:*leave-edit-mode-hooks*
   #:writer-of

   ;; progess widget
   #:progress-of

   ;; slider widget
   #:value-of

   ;; viewport widget
   #:render-of

   ;; style parameters
   #:*stylesheet*

   #:*colour-transparent*
   #:*colour-white*
   #:*colour-black*

   #:*button-background-colour*
   #:*button-foreground-colour*

   #:*button-depressed-background-colour*

   #:*input-background-colour*
   #:*red-input-background-colour*
   #:*green-input-background-colour*
   #:*blue-input-background-colour*
   #:*yellow-input-background-colour*
   #:*magenta-input-background-colour*
   #:*cyan-input-background-colour*

   #:*generic-background-colour*
   #:*generic-foreground-colour*

   #:*translucent-generic-background-colour*

   ;;; Convenience macros
   ;; containers
   #:make-hbox
   #:make-vbox
   #:make-grid

   ;; buttons
   #:make-scroll-button
   #:make-callback-toggle-button
   #:make-toggle-button
   #:make-clampf-button
   #:make-simple-numeric-button
   #:make-button

   ;; textual
   #:make-text-block
   #:make-label
   #:make-dynamic-text

   ;; high-level
   #:make-tab-panel
   #:progress-screen))


(in-package #:cowl)

(defmacro compiler-opts ()
  '(declaim (optimize (debug 3) 
	     (safety 3)
	     (compilation-speed 0)
	     (speed 0)
	     (space 0))))

