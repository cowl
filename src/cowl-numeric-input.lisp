(in-package #:cowl)

(defclass numeric-input (input)
  ((number-type :initarg :number-type :initform 'single-float :accessor number-type :type (or symbol cons)
                :documentation "A typespec of the type to coerce the number into on write."))
  (:documentation "Widget for editable numbers.
The writer initarg function will be automatically wrapped in 
the initialize-instance so that it is given a number as the first argument."))

(defmethod initialize-instance :after ((numeric-input numeric-input) &key writer)
  (when writer
    (setf (slot-value numeric-input 'writer)
          (lambda (val)
            (let ((rval (read-from-string val)))
              (when (numberp rval)
                (funcall writer (coerce rval (number-type numeric-input)))))))))

(defmethod accept-char-p ((numeric-input numeric-input) character)
  (or (eql #\. character)
      (eql #\- character)
      (digit-char-p character)))