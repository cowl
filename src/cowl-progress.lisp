(in-package #:cowl)

(defclass progress (widget)
  ((progress :type single-float :initform 0.0 :initarg :progress :accessor progress-of))
  (:documentation "Progress meter widget."))

(defmethod draw ((progress progress))
  (call-next-method)
  (apply #'gl:color-4f (foreground-colour-of progress))
  (gl:disable gl:+texture-2d+)
  (gl:rect-f 0 (current-size-of (y progress))
             (* (progress-of progress)
                (current-size-of (x progress)))
             0))