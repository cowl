
(in-package #:cowl)

(defclass graph (widget)
  ((graph-data :initarg :data :accessor graph-data-of :documentation "function returning a list of points")
   (running-min :initform 0.0 :accessor running-min-of :documentation "The minimum value to display")
   (running-max :initform 0.0 :accessor running-max-of :documentation "The minimum value to display")
   (pixels :accessor pixels))
  (:documentation "Graph for the on-screen display."))

(defmethod initialize-instance :after ((graph graph) &key &allow-other-keys)
  (with-accessors ((pixels pixels) (x x) (y y)) graph
    (let ((-pixels (setf pixels (cffi:foreign-alloc 'gl:int
                                                    :count (* (round (ideal-size-of x))
                                                              (round (ideal-size-of y)))))))
      (tg:finalize graph (lambda () (cffi:foreign-free -pixels))))))
    
(defmethod graph-data-of ((graph graph))
  (with-slots (graph-data) graph
    (if (functionp graph-data)
        (funcall graph-data)
        graph-data)))

(defmethod draw ((graph graph))
  (call-next-method)
  (unless (slot-boundp graph 'graph-data) (return-from draw))
  (with-accessors ((x x) (y y)
                   (running-min running-min-of) (running-max running-max-of)
                   (colour foreground-colour-of)
                   (data graph-data-of)) graph
    (setf running-min (apply #'min (cons running-min data))
          running-max (apply #'max (cons (1- running-max) data)))
    (apply #'gl:color-4f colour)
    (gl:with-push-attrib (gl:+line-bit+)
      (gl:enable gl:+line-smooth+)
      (gl:line-width 1.0)
      (gl:with-begin gl:+line-strip+
        (loop for value in data
           for ix from 0.0 do
           (gl:vertex-2f ix (coerce (- (current-size-of y)
                                       (* (current-size-of y)
                                          (/ (- value running-min) 
                                             (- running-max running-min))))
                                    'single-float)))))))