
(in-package #:cowl)

(defmacro make-hbox ((&rest container-keyargs) &body contents) 
  `(make-instance 'container
                  ,@container-keyargs
                  :contents (list (list ,@contents))
                  :column-widths (make-list ,(list-length contents) :initial-element '(:expand . 10))
                  :style :hbox))

(defmacro make-vbox ((&rest container-keyargs) &body contents) 
  `(make-instance 'container
                  ,@container-keyargs
                  :contents (list ,@(mapcar #'(lambda (widget) `(list ,widget)) contents))
                  :row-heights (make-list ,(list-length contents) :initial-element '(:expand . 10))
                  :style :vbox))

(defmacro make-grid ((&rest container-keyargs) &body contents) 
  `(make-instance 'container
                  ,@container-keyargs
                  :contents (list ,@(mapcar #'(lambda (row) `(list ,@row)) contents))
                  :row-heights (make-list ,(list-length contents) :initial-element '(:expand . 10))
                  :column-widths (make-list ,(list-length (first contents)) :initial-element '(:expand . 10))
                  :style :grid))

(defmacro make-scroll-button ((&rest text-button-keyargs) 
                              format-text variable-place
                              &key (update-p t)
                              (wheel-delta-form 0.1)
                              minimum
                              maximum)
  `(labels ((numeric-writer (new-value &optional self)
              (prog1 (if ,update-p
                         (setf ,variable-place
                               ,(cond
                                 ((and minimum maximum)
                                  `(max ,minimum (min ,maximum new-value)))
                                 (minimum `(max ,minimum new-value))
                                 (maximum `(min ,maximum new-value))
                                 (t 'new-value)))
                         ,variable-place)
                (when self (update-text self)))))
     (make-instance 'numeric-input ,@text-button-keyargs 
                    :writer #'numeric-writer
                    :text-updater #'(lambda (self) 
                                      (declare (ignorable self))
                                      (if ,update-p (format nil ,format-text ,variable-place) "-"))
                    :middle-click
                    (lambda (self)
                      (declare (ignorable self))
                      (funcall #'numeric-writer
                               (+ ,variable-place (* ,wheel-delta-form 0.1)))
                      t)
                    :right-click
                    (lambda (self)
                      (declare (ignorable self))
                      (funcall #'numeric-writer
                               (- ,variable-place (* ,wheel-delta-form 0.1)))
                      t)
                    :wheel
                    (lambda (self zrel) 
                      (declare (ignorable self))
                      (funcall #'numeric-writer
                               (+ ,variable-place (* zrel ,wheel-delta-form)))
                      t))))

(defmacro make-callback-toggle-button ((&rest text-button-initargs) 
                                       reader-function writer-function)
  `(flet ((update-style (self val)
            (set-widget-style self (if val :depressed-button :button))))
     (let ((button (make-instance
             'text-button ,@text-button-initargs
             :left-click
             #'(lambda (self)
                 (update-style self (funcall ,writer-function
                                             (not (funcall ,reader-function))))
                 t))))
       (update-style button (funcall ,reader-function))
       button)))

(defmacro make-toggle-button (place &rest text-button-initargs)
  `(labels ((reader () ,place)
            (writer (val) (setf ,place val)))
     (declare (inline reader writer))
     (make-callback-toggle-button 
      (,@text-button-initargs)
      #'reader #'writer)))




(defmacro make-clampf-button ((&rest text-button-keyargs) format-text variable-place)
  `(make-scroll-button (,@text-button-keyargs)
                      ,format-text ,variable-place
                      :minimum 0.0
                      :maximum 1.0))

(defmacro make-simple-numeric-button ((&rest text-button-keyargs)
                                      format-text
                                      reader
                                      writer
                                      &key
                                      (delta-form 10)
                                      (middle-click-value `(- ,reader (/ ,delta-form 10)))
                                      (right-click-value `(+ ,reader (/ ,delta-form 10)))
                                      (mouse-wheel-value `(+ ,reader (* zrel ,delta-form))))
  "Make a simple numeric button, format-text is the way to format it.
reader is a form to evaluate to get the value.
writer is a function with a single argument to set the new value.
delta-form is a form to increment/decrement the value by if no click/wheel forms are given.
right/middle-click-value are a new form that returns a new value for when that event happens. 
mouse-wheel-value is the same, but zrel is bound to the wheel delta.
For these forms, the value of self is bound to the numeric-button."
  `(make-instance 'numeric-input ,@text-button-keyargs
                  :text-updater #'(lambda (self)
                                    (declare (ignorable self))
                                    (format nil ,format-text ,reader))
                  :writer ,writer
                  :middle-click
                  #'(lambda (self)
                      (declare (ignorable self))
                      (funcall ,writer ,middle-click-value)
                      (update-text self)
                      t)
                  :right-click
                  #'(lambda (self)
                      (declare (ignorable self))
                      (funcall ,writer ,right-click-value)
                      (update-text self)
                      t)
                  :wheel
                  #'(lambda (self zrel)
                      (declare (ignorable self))
                      (funcall ,writer ,mouse-wheel-value)
                      (update-text self)
                      t)))




(defmacro make-button ((&rest text-button-keyargs) text &body left-click-forms)
  `(make-instance 'text-button ,@text-button-keyargs 
                  :text ,text
                  :left-click #'(lambda (self) 
                                  (declare (ignorable self))
                                  ,@left-click-forms
                                  t)))


(defmacro make-text-block ((&rest text-initargs) text)
  `(make-instance 'text ,@text-initargs :wrap t :text ,text :style :text-block :width 600))

(defmacro make-label (text &rest text-initargs)
  `(make-instance 'text ,@text-initargs :wrap nil :text ,text :style :label))

(defmacro make-dynamic-text ((&rest text-initargs) &body forms)
  `(make-instance 'text ,@text-initargs 
                  :text-updater #'(lambda (self)
                                    (declare (ignorable self))
                                    ,@forms)))

(defmacro make-tab-panel (&body text-contents)
  `(let ((tab-content-callbacks
          (make-array ,(length text-contents)
                      :initial-contents
                      (list ,@(loop for text-content in text-contents collecting
                                   (if (eql (second text-content) :function)
                                       `(function ,(third text-content))
                                       `(constantly ,@(cdr text-content)))))))
         (tab-panel)
         (buttons (make-array ,(length text-contents))))
     (flet ((update (selected)
              (loop for widget across buttons
                 for i from 0 do
                 (if (= i selected)
                     (set-widget-style widget :depressed-button)
                     (set-widget-style widget :button)))
              (set-cell tab-panel 1 0 (funcall (elt tab-content-callbacks selected)))))
       (setf tab-panel
             (make-vbox (:row-heights '((:expand . 10) (:weight . 1.0)))
               (make-hbox ()
                 ,@(loop for text-label in (mapcar #'first text-contents) 
                      for i from 0 collecting
                      `(setf (aref buttons ,i)
                            (make-button () ,text-label
                               (update ,i)))))
               nil))
       (update 0)
       tab-panel)))


(defun progress-screen (title progress)
  (destructuring-bind (display-width display-height)
      (let ((viewport (list 0 0 0 0)))
        (gl:get-integerv gl:+viewport+ viewport)
        (cddr viewport))
    (let* ((progress-meter 
            (make-instance 'progress :width display-width :height 20))
           (*root-widget*
            (make-vbox (:x 0 :y 0 :width display-width :height display-height
                           :row-heights `((:expand . ,(/ display-width 3))
                                          (:expand . 20)
                                          (:expand . 20)
                                          (:expand . ,(/ display-width 3))))
              nil
              (make-hbox (:column-widths '((:weight . 1.0) (:expand . 10.0) (:weight . 1.0)))
                nil
                (make-instance 'text :text title)
                nil)
              progress-meter
              nil))
           (*ignore-events* t))
      (declare (special *ignore-events*))
      (gl:clear (logior gl:+color-buffer-bit+))
      (setf (progress-of progress-meter) progress)
      (draw-cowl))))

