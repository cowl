(in-package #:cowl)

(defclass slider (widget)
  ((value :initarg :value :initform 0.0
          :type (or (float 0.0 1.0)
                    (cons (float 0.0 1.0)
                          (float 0.0 1.0)))
          :accessor value-of
          :documentation "The position along the widget where the handle is.")
   (direction :initarg :direction :type (member :vertical :horizontal :both)
              :initform :vertical :reader direction-of
              :documentation "The direction of movement for this slider.")
   (shuttle :initarg :shuttle :type widget :reader shuttle-of
            :documentation "The widget that is dragged along the slider.
Can be specified as a custom widget, by default is just blank.")))

(defmethod initialize-instance :after ((slider slider) &key width height)
  (with-accessors ((direction direction-of)) slider
    (unless (slot-boundp slider 'shuttle)
      (setf (slot-value slider 'shuttle)
            (make-instance 'widget
                           :width (case direction
                                    (:vertical (current-size-of (x slider)))
                                    (:both 10)
                                    (:horizontal 5))
                           :height (case direction
                                     (:horizontal (current-size-of (y slider)))
                                     (:both 10)
                                     (:vertical 5))
                           :style :shuttle)))
    (when (eql direction :both)
      (unless (consp (value-of slider))
        (setf (value-of slider)
              (cons (value-of slider)
                    (value-of slider)))))
    (when (eql (direction-of slider)
               :vertical))))

(defmethod accept-focus-p ((slider slider)) t)

(defmethod layout ((slider slider))
  (macrolet ((set-offset (slider-widget shuttle-widget value)
               `(setf (current-offset-of ,shuttle-widget)
                      (round
                       (* ,value
                          (- (current-size-of ,slider-widget)
                             (total-current-size-of ,shuttle-widget)))))))
    (with-accessors ((shuttle shuttle-of)
                     (value value-of)) slider
      (layout shuttle)
      (case (direction-of slider)
        (:horizontal
         (set-offset (x slider) (x shuttle) value))
        (:vertical
         (set-offset (y slider) (y shuttle) value))
        (:both
         (set-offset (x slider) (x shuttle) (car value))
         (set-offset (y slider) (y shuttle) (cdr value)))))))

(defmethod (setf value-of) (val (slider slider))
  (prog1 (setf (slot-value slider 'value)
               (case (direction-of slider)
                 (:both
                  (cons (max 0.0 (min 1.0 (car val)))
                        (max 0.0 (min 1.0 (cdr val)))))
                 ((:vertical :horizontal)
                  (max 0.0 (min 1.0 val)))))
    (layout slider)))

(defmethod shuttle-width-of ((slider slider))
  (/ (total-current-size-of (x (shuttle-of slider)))
     (current-size-of (x slider))))

(defmethod shuttle-height-of ((slider slider))
  (/ (total-current-size-of (y (shuttle-of slider)))
     (current-size-of (y slider))))

(defmethod (setf shuttle-width-of) (value (slider slider))
  (prog1 (setf (total-current-size-of (x (shuttle-of slider)))
               (* value (current-size-of (x slider))))
    (layout slider)))

(defmethod (setf shuttle-height-of) (value (slider slider))
  (prog1 (setf (total-current-size-of (y (shuttle-of slider)))
               (* value (current-size-of (y slider))))
    (layout slider)))

(defmethod update-text ((slider slider))
  (update-text (shuttle-of slider)))

(defmethod ideal-width-of ((slider slider))
  (case (direction-of slider)
    (:vertical
     (total-ideal-width-of (shuttle-of slider)))
    (otherwise
     (call-next-method))))

(defmethod ideal-height-of ((slider slider))
  (case (direction-of slider)
    (:horizontal
     (total-ideal-height-of (shuttle-of slider)))
    (otherwise
     (call-next-method))))

(defmethod draw ((slider slider))
  (with-accessors ((shuttle shuttle-of)) slider
    (gl:with-push-matrix
      (gl:translate-f (current-offset-of (x shuttle))
                      (current-offset-of (y shuttle))
                      0.0)
      (draw shuttle))))

(defun slider-nudge (slider delta-pixels &key (prefer :vertical))
  (declare (type slider slider))
  (declare (type real delta-pixels))
  (declare (type (member :vertical :horizontal) prefer))
  (macrolet ((delta (direction)
               `(/ delta-pixels
                   (- (current-size-of (,direction slider))
                      (total-current-size-of (,direction (shuttle-of slider)))))))
    (case (direction-of slider)
      (:both
       (if (eql prefer :horizontal)
           (setf (car (value-of slider))
                 (max 0.0 (min 1.0 (+ (car (value-of slider)) (delta x)))))
           (setf (cdr (value-of slider))
                 (max 0.0 (min 1.0 (+ (cdr (value-of slider)) (delta y))))))
       (layout slider))
      (:horizontal (incf (value-of slider) (delta x)))
      (:vertical   (incf (value-of slider) (delta y))))))

(defun slider-set-centre (slider new-centre)
  (declare (type slider slider))
  (declare (type (or real (cons real real)) new-centre))
  (macrolet ((pix-to-value (direction pix)
               `(/ (- ,pix (/ (total-current-size-of (,direction (shuttle-of slider))) 2))
                  (- (current-size-of (,direction slider))
                     (total-current-size-of (,direction (shuttle-of slider)))))))
    (case (direction-of slider)
      (:both       (setf (value-of slider) (cons (pix-to-value x (car new-centre))
                                                 (pix-to-value y (cdr new-centre)))))
      (:horizontal (setf (value-of slider) (pix-to-value x new-centre)))
      (:vertical   (setf (value-of slider) (pix-to-value y new-centre))))))


(defun slider-mouse-motion (x y)
  "This is a mouse-motion-handler function for when "
  (destructuring-bind (xoff yoff) (cumulative-offset-of *focused-widget*)
   (case (direction-of *focused-widget*)
     (:horizontal
      (slider-set-centre *focused-widget* (- x xoff)))
     (:vertical
      (slider-set-centre *focused-widget* (- y yoff)))
     (:both
      (slider-set-centre *focused-widget* (cons (- x xoff)
                                                (- y yoff))))))
  t)

(defun slider-motion-handle-mouse-button (button press x y)
  "This is a mouse-button-handler to check when the slider motion is ended."
  (declare (type symbol button))
  (declare (type boolean press))
  (declare (ignore x y))
  (when (and (not press) (eql button :left))
    (setf *mouse-motion-handlers* (delete 'slider-mouse-motion *mouse-motion-handlers*)
          *mouse-button-handlers* (delete 'slider-end-motion *mouse-button-handlers*))
    t))

(defun slider-end-motion ()
  "This is a mouse-button-handler to check when the slider motion is ended."
  (setf *mouse-motion-handlers* (delete 'slider-mouse-motion *mouse-motion-handlers*)
        *mouse-button-handlers* (delete 'slider-end-motion *mouse-button-handlers*)))

(defmethod focus ((slider slider)))

(defmethod blur ((slider slider))
  (slider-end-motion))

(defun slider-start-motion (slider)
  (focus slider)
  (push 'slider-mouse-motion *mouse-motion-handlers*)
  (push 'slider-motion-handle-mouse-button *mouse-button-handlers*))

(defmethod handle-mouse-button ((slider slider) button press x y)
  (declare (type boolean press))
  (declare (type symbol button))
  (declare (type (real 0) x y))
  (with-widget-box-bindings slider
    (or (when (and press
                   (>= x bx00) (<= x bx11)
                   (>= y by00) (<= y by11))
          (case button
            (:left
             (let ((x (- x bx01 (current-offset-of (x (shuttle-of slider)))))
                   (y (- y by01 (current-offset-of (y (shuttle-of slider))))))
               (with-widget-box-bindings (shuttle-of slider)
                 (when (and (>= x bx00) (<= x bx11)
                            (>= y by00) (<= y by11))
                   (slider-start-motion slider)))))
            (:middle
             (slider-nudge slider -0.5 :prefer :horizontal))
            (:right
             (slider-nudge slider 0.5 :prefer :horizontal))))
        ;; this is probably defunct now
        (let ((x (- x bx01))
              (y (- y by01)))
          (handle-mouse-button (shuttle-of slider) button press x y)))))


(defmethod handle-mouse-wheel ((slider slider) zrel x y)
  (with-widget-box-bindings slider
    (when (and (>= x bx00) (<= x bx11)
               (>= y by00) (<= y by11))
      (slider-nudge slider (- zrel) :prefer :vertical)
      t)))

