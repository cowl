(in-package #:cowl)
(compiler-opts)

(defclass text (widget)
  ((font :initarg :font :initform (default-font) :accessor font-of
         :documentation "font object to render the text in")
   (text :type string :initarg :text :accessor text-of
         :documentation "The text to display.")
   (text-updater :initarg :text-updater :type (function (widget) string) :accessor text-updater-of
                 :documentation "A function that can be associated with this input from which the
text can be derived.")
   (lines :type list :accessor lines-of
          :documentation "The actual lines of text to display. 
Use the text property unless you really want to explicitely set the lines.")
   (wrap :type boolean :initform t :initarg :wrap :accessor wrap
         :documentation "Whether or not to wrap the text to the current size."))
  (:documentation "Wrappable text object on the on-screen-display system."))


(defgeneric display-text-of (text)
  (:documentation "Can be used to display a different set of characters than is input"))

(defmethod display-text-of ((text text))
  (text-of text))


(defmethod initialize-instance :after ((text text) &key height width (wrap nil wrap-p))
  (unless height
    (setf (current-size-of (y text))
          (setf (ideal-size-of (y text))
                (ftgl:get-font-line-height (font-of text)))))
  ;; if wrap is not specified, 
  (unless wrap-p
    (if width
        ;; wrap if width is specified
        (setf (wrap text) t)
        ;; don't wrap if no width is specified
        (setf (wrap text) nil)))

  ;; if we don't have text, but we do have a text-updater
  (when (and (not (slot-boundp text 'text))
             (slot-boundp text 'text-updater))
    (setf (text-of text)
          (funcall (text-updater-of text) text)))

  ;; if we aren't wrapping, no width is specified and we have text
  (when (and (not wrap) (not width)
             (slot-boundp text 'text))
    ;; set the ideal-width to the size of the text ; ;
    (setf (current-size-of (x text))
          (setf (ideal-size-of (x text))
                (ftgl:get-font-advance (font-of text) (display-text-of text))))))


(defmethod update-text ((text text))
  "Calls the updater callback if one exists and sets to as the current text"
  (when (slot-boundp text 'text-updater)
   (setf (text-of text)
         (funcall (slot-value text 'text-updater) text))))

(defmethod cursor-x-y-to-position ((text text) ix iy)
  (+ (min ix (length (elt (lines-of text) iy)))
     (loop for y below iy for line in (lines-of text) summing (length line))))

(defmethod ideal-width-of ((text text))
  (if (wrap text)
      (call-next-method)
      (max (call-next-method)
	   (ftgl:get-font-advance (font-of text) (display-text-of text)))))


(defmethod ideal-height-of ((text text))
  (if (wrap text)
      (nth-value 1 (font-text-wrap (font-of text) (display-text-of text) (ideal-size-of (x text))))
      (max (call-next-method)
	   (ftgl:get-font-line-height (font-of text)))))

(defmethod layout ((text text))
  (with-accessors ((font font-of) (lines lines-of) (x x) (y y) (wrap wrap) (text display-text-of)) text
    (if wrap
      (setf lines (font-text-wrap font text (current-size-of x)))
      (setf lines (list text)))))


(defmethod draw ((text text))
  (when (slot-boundp text 'lines)
    (with-accessors ((font font-of)
                     (lines lines-of)
                     (foreground-colour foreground-colour-of)) text
      (apply #'gl:color-4f foreground-colour)
      (let ((font-height (round (ftgl:get-font-line-height font))))
        (gl:rotate-f 180.0 1.0 0.0 0.0)
        (loop for iy downfrom (- 0 font-height (round (ftgl:get-font-descender font))) by font-height
           for line in lines
           do (gl-print font 0 iy line)))))
  (when (next-method-p) (call-next-method)))