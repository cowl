(in-package #:cowl)

(defclass input (text-button)
  ((writer :initarg :writer :type (function (t))
           :accessor writer-of
           :documentation "Whenever edit mode is left and the text confirmed, this callback is called
with the new string."))
  (:documentation "Widget for editable text. "))

(defclass password-input (input)
  ()
  (:documentation "Purely specialized to not echo the text back"))

(defparameter *edit-mode-cursor-positions* (list 0 0)
  "A list of two values of the current cursor position. If the values are different,
this signifies a selection.")
(declaim (type (cons (integer 0) (cons (integer 0) null)) *edit-mode-cursor-positions*))

(defparameter *edit-mode-cursor-blink-state* t
  "Whether the cursor is currently blinked on or off")
(declaim (type boolean *edit-mode-cursor-blink-state*))

(defparameter *edit-mode-cursor-blink-time* (/ internal-time-units-per-second 2)
  "Setting for how long the cursor should blink before toggling")

(defparameter *edit-mode-cursor-blink-last-change* 0
  "Current time of the last state change of blink")

(defparameter *edit-mode-text-memento* "You should never see this"
  "The text before we entered edit-mode (for reverting back if the user cancels editing)")
(declaim (type string *edit-mode-text-memento*))

(defparameter *edit-mode-drag-anchors* nil
  "If not-nil, the first is the initial position of the drag and the second
Or nil if there is no drag currently happening.")
(declaim (type (or (cons (integer 0) (cons (integer 0) null)) null) *edit-mode-drag-anchors*))

(defparameter *enter-edit-mode-hooks* nil
  "A list of functions called BEFORE entering edit mode.
This can be used, eg. for ensuring that a character
reader event handler is registered in the client application.")
(defparameter *leave-edit-mode-hooks* nil
  "A list of functions called AFTER leaving edit mode.
This can be used for undoing the effects of 
*enter-edit-mode-hooks*, when editing is finished.")

(defmethod display-text-of ((input password-input))
  (make-string (length (text-of input)) :initial-element #\*))

(defmethod accept-focus-p ((input input))
  (declare (ignore input))
  t)

(defmethod focus ((input input))
  "Sets up edit mode with input as the active widget."
  (assert (eql input *focused-widget*))
  (mapc #'funcall *enter-edit-mode-hooks*)
  (setf *edit-mode-text-memento* (text-of *focused-widget*))
  (setf *edit-mode-cursor-positions* (list 0 (length (text-of input))))
  (setf *edit-mode-cursor-blink-state* t)
  (setf *edit-mode-cursor-blink-last-change* (get-internal-real-time)))

(defmethod finish-edit-mode ((input input) confirm-edit)
  "Hook method called after the text input is complete, confirmed or cancelled.
If confirm-edit is true, call the writer callback, else revert text to previous state."
  (if confirm-edit
      (when (slot-boundp input 'writer)
        (funcall (slot-value input 'writer) (text-of input)))
      (setf (text-of input) *edit-mode-text-memento*)))

(defmethod blur ((input input))
  "Invokes leave-edit-mode."
  (assert (eql input *focused-widget*))
  (leave-edit-mode t))

(defun leave-edit-mode (&optional (confirm-edit t))
  "A command to drop the focused widget out of edit-mode.
Invokes finish-edit-mode."
  (declare (type boolean confirm-edit))
  (finish-edit-mode *focused-widget* confirm-edit)
  (setf *focused-widget* nil)
  (mapc #'funcall *leave-edit-mode-hooks*))

(defmethod offset-to-char-x-char-y ((input input) ox oy)
  "Converts a cursor offset to a 'char coordinate', of the line and colum number
of wrapped text."
  (let* ((iy (max 0 (min (1- (length (lines-of input)))
                         (floor oy (ftgl:get-font-line-height (font-of input))))))
         (ix (font-string-character-position (font-of input)
                                             (elt (lines-of input) iy)
                                             (max 0 (floor ox)))))
    ;; (format t "cursor at ~a,~a~%" ix iy)
    (cursor-x-y-to-position input ix iy)))

(defun input-selection-drag-mouse-motion (mx my)
  "Handler for mouse motion while an input is having a selection dragged."
  (destructuring-bind (cx cy) (cumulative-offset-of *focused-widget*)
    (let ((x (- mx cx))
          (y (- my cy)))
      ;; (format t "Dragging at ~a,~a which is ~a,~a offset from ~a,~a~%" (glfw:get-mouse-pos) x y cx cy)
      (setf (second *edit-mode-drag-anchors*)
            (offset-to-char-x-char-y *focused-widget* x y))
      (setf (edit-mode-cursor-positions) *edit-mode-drag-anchors*)))
  t)

;; ?? mouse-button-handlers mouse-motion-handlers
(defun input-selection-drag-finish (button press x y)
  (when (and (not press)
             (eql button :left))
    (setf *mouse-button-handlers* (delete #'input-selection-drag-finish *mouse-button-handlers*))
    (setf *mouse-motion-handlers* (delete #'input-selection-drag-mouse-motion *mouse-motion-handlers*))
    t))

(defmethod handle-mouse-button ((input input) button press x y)
  (if (and press (eql button :left))
      (progn
        (focus input)
        (setf *edit-mode-drag-anchors*
              (make-list 2 :initial-element
                         (offset-to-char-x-char-y input
                                                  (- x
                                                     (before-margin-of (x input))
                                                     (before-border-width-of (x input))
                                                     (before-padding-of (x input)))
                                                  (- y
                                                     (before-margin-of (y input))
                                                     (before-border-width-of (y input))
                                                     (before-padding-of (y input))))))
        (setf (edit-mode-cursor-positions) *edit-mode-drag-anchors*)
        ;; (format t "anchors: ~a; cursor positions: ~a~%" *edit-mode-drag-anchors* *edit-mode-cursor-positions*)
        (pushnew #'input-selection-drag-mouse-motion *mouse-motion-handlers*)
        (pushnew #'input-selection-drag-finish *mouse-button-handlers*)
        t)
      (call-next-method)))


(defun edit-mode-cursor-positions-fix ()
  (setf *edit-mode-cursor-positions* (sort *edit-mode-cursor-positions* #'<)
        (first *edit-mode-cursor-positions*) (max 0 (first *edit-mode-cursor-positions*))
        (second *edit-mode-cursor-positions*) (min (length (display-text-of *focused-widget*))
                                                   (second *edit-mode-cursor-positions*))))

(defun edit-mode-cursor-positions () *edit-mode-cursor-positions*)
(defun (setf edit-mode-cursor-positions) (val)
  (when *focused-widget*
    (setf *edit-mode-cursor-positions*
          (mapcar #'(lambda (val) (mod val (1+ (length (display-text-of *focused-widget*)))))
                  (if (listp val) val (list val val)))))
  (edit-mode-cursor-positions-fix)
  val)


(defun cursor-position (cursor-index)
  (let ((lines-length 0))
    (loop for line in (lines-of *focused-widget*)
       for i from 0
       do
       (when (and (>= cursor-index lines-length)
                  (<= cursor-index (+ (length line) lines-length)))
         (return-from cursor-position (list (- cursor-index lines-length) i)))
       (incf lines-length (length line)))
    (list (length (car (last (lines-of *focused-widget*))))
          (1- (length (lines-of *focused-widget*))))))

(defun edit-mode-update-cursor-blink ()
  (when (> (- (get-internal-real-time)
              *edit-mode-cursor-blink-last-change*)
           *edit-mode-cursor-blink-time*)
    (setf *edit-mode-cursor-blink-last-change* (get-internal-real-time)
          *edit-mode-cursor-blink-state* (not *edit-mode-cursor-blink-state*)))
  *edit-mode-cursor-blink-state*)

(defmethod detach ((input input))
  (when (eql input *focused-widget*)
    (leave-edit-mode nil)))

(defmethod draw-cursor ((input input))
  (gl:disable gl:+texture-2d+)
  (apply #'gl:color-4f (foreground-colour-of input))
  (if (apply #'= *edit-mode-cursor-positions*)
      (progn
        (unless (edit-mode-update-cursor-blink)
          (return-from draw-cursor))
        (gl:begin gl:+lines+))
      (gl:begin gl:+quads+))
  (labels ((cursor-x-offset (cursor-pos)
             (destructuring-bind (cursor-x cursor-y) cursor-pos
               (font-string-cursor-offset
                (font-of input)
                (elt (lines-of input) cursor-y)
                cursor-x)))
           (cursor-y-offset (cursor-pos)
             (* (second cursor-pos) (ftgl:get-font-line-height (font-of input))))
           (vertex-pair (cursor-pos &optional down)
             (let ((cursor-x-offset (cursor-x-offset cursor-pos))
                   (cursor-y-offset (cursor-y-offset cursor-pos)))
               (if down
                   (progn
                     (gl:vertex-2f cursor-x-offset cursor-y-offset)
                     (gl:vertex-2f cursor-x-offset (+ cursor-y-offset (ftgl:get-font-line-height (font-of input)))))
                   (progn
                     (gl:vertex-2f cursor-x-offset (+ cursor-y-offset (ftgl:get-font-line-height (font-of input))))
                     (gl:vertex-2f cursor-x-offset cursor-y-offset))))))

    (let ((cursor-pos-front (cursor-position (first *edit-mode-cursor-positions*))))
      (vertex-pair cursor-pos-front t)
      (unless (apply #'= *edit-mode-cursor-positions*)
        (let ((cursor-pos-back (cursor-position (second *edit-mode-cursor-positions*))))
          (do ((i (second cursor-pos-front) (1+ i)))
              ((>= i (second cursor-pos-back)))
            (vertex-pair (list (length (elt (lines-of *focused-widget*) i)) i))
            (vertex-pair (list 0 (1+ i)) t))
          (vertex-pair cursor-pos-back)))))
  (gl:end))

(defmethod draw ((input input))
  (when (eql input *focused-widget*)
    (draw-cursor input))
  (call-next-method))

(defun string-insert (string replacement position)
  "Returns a string with the character at the position, or between the cons pair of positions, in string."
  (cond
    ((typep position '(cons integer (cons integer null)))
     (concatenate 'string
                  (subseq string 0 (first position))
                  replacement
                  (subseq string (second position))))
    ((typep position 'integer)
     (concatenate 'string
                  (subseq string 0 position)
                  replacement
                  (subseq string position)))))

(defmethod (setf text-of) (val (input input))
  (call-next-method)
  (when *focused-widget*
    (setf (edit-mode-cursor-positions)
          (list (max (first *edit-mode-cursor-positions*) (length val))
                (max (second *edit-mode-cursor-positions*) (length val))))
    (when *edit-mode-drag-anchors*
      (setf (first *edit-mode-drag-anchors*) (max (first *edit-mode-drag-anchors*) (length val))
            (second *edit-mode-drag-anchors*) (max (second *edit-mode-drag-anchors*) (length val))))))

(defun edit-mode-backspace-text ()
  (unless (apply #'= (cons 0 *edit-mode-cursor-positions*))
    (setf (first *edit-mode-cursor-positions*)
          (min (first *edit-mode-cursor-positions*)
               (1- (second *edit-mode-cursor-positions*))))
    (with-accessors ((text text-of)) *focused-widget*
      (psetf text (string-insert text nil *edit-mode-cursor-positions*)
             (edit-mode-cursor-positions) (first *edit-mode-cursor-positions*)))))

(defun edit-mode-delete-text ()
  (with-accessors ((text text-of)) *focused-widget*
    (unless (apply #'= (cons (length text) *edit-mode-cursor-positions*))
      (setf (second *edit-mode-cursor-positions*)
            (max (second *edit-mode-cursor-positions*)
                 (1+ (first *edit-mode-cursor-positions*))))
      (psetf text (string-insert text nil *edit-mode-cursor-positions*)
             (edit-mode-cursor-positions) (first *edit-mode-cursor-positions*)))))

(defun edit-mode-insert-text (char)
  (with-accessors ((text text-of)) *focused-widget*
    (psetf text (string-insert text (list char) *edit-mode-cursor-positions*)
           (edit-mode-cursor-positions) (1+ (first *edit-mode-cursor-positions*)))))

(defun edit-mode-backwards-word (position)
  (if (zerop position)
      0
      (or (position-if #'blankp (text-of *focused-widget*) :from-end t :end position)
          0)))

(defun edit-mode-forwards-word (position)
  (if (= position (length (text-of *focused-widget*)))
      (length (text-of *focused-widget*))
      (or (position-if #'blankp (text-of *focused-widget*) :start (1+ position))
          (length (text-of *focused-widget*)))))

(defun edit-mode-move-line (position delta)
  (destructuring-bind (cx cy) (cursor-position position)
    (let ((x-offset (font-string-cursor-offset (font-of *focused-widget*)
                                               (elt (lines-of *focused-widget*)
                                                    cy)
                                               cx)))
      (setf cy (mod (+ cy delta) (length (lines-of *focused-widget*))))
      (+ (loop for i below cy for line in (lines-of *focused-widget*)
            summing (length line))
         (font-string-character-position (font-of *focused-widget*)
                                         (elt (lines-of *focused-widget*) cy)
                                         x-offset)))))

(defmethod accept-char-p ((input input) character)
  (graphic-char-p character))


(defmethod handle-key ((input input) key press)
  (when press
    (setf *edit-mode-cursor-blink-last-change* (get-internal-real-time)
          *edit-mode-cursor-blink-state* t)
    (etypecase key
      (character
       (when (accept-char-p input key)
         (edit-mode-insert-text key)))
      (symbol
       (case key
         (:home
          (if (intersection *held-keys* '(:lshift :rshift))
              (setf (first *edit-mode-cursor-positions*) 0)
              (setf (edit-mode-cursor-positions) 0))
          t)
         (:end
          (if (intersection *held-keys* '(:lshift :rshift))
              (setf (second *edit-mode-cursor-positions*) (length (text-of input)))
              (setf (edit-mode-cursor-positions) (length (text-of input))))
          t)
         (:backspace (edit-mode-backspace-text) t)
         (:del (edit-mode-delete-text) t)
         ((:enter :kp-enter) (leave-edit-mode t) t)
         (:esc (leave-edit-mode nil) t)
         (:left
          (cond
            ((intersection *held-keys* '(:lshift :rshift))
             (setf (first *edit-mode-cursor-positions*)
                   (if (intersection *held-keys* '(:lctrl :rctrl))
                       (edit-mode-backwards-word (first *edit-mode-cursor-positions*))
                       (1- (first *edit-mode-cursor-positions*))))
             (edit-mode-cursor-positions-fix))
            ((intersection *held-keys* '(:lctrl :rctrl))
             (setf (edit-mode-cursor-positions)
                   (edit-mode-backwards-word (first *edit-mode-cursor-positions*))))
            (t
             (setf (edit-mode-cursor-positions) (1- (first *edit-mode-cursor-positions*)))))
          t)
         (:right
          (cond
            ((intersection *held-keys* '(:lshift :rshift))
             (setf (second *edit-mode-cursor-positions*)
                   (if (intersection *held-keys* '(:lctrl :rctrl))
                       (edit-mode-forwards-word (second *edit-mode-cursor-positions*))
                       (1+ (second *edit-mode-cursor-positions*))))
             (edit-mode-cursor-positions-fix))
            ((intersection *held-keys* '(:lctrl :rctrl))
             (setf (edit-mode-cursor-positions)
                   (edit-mode-forwards-word (second *edit-mode-cursor-positions*))))
            (t
             (setf (edit-mode-cursor-positions) (1+ (second *edit-mode-cursor-positions*)))))
          t)
         (:up
          (cond
            ((intersection *held-keys* '(:lshift :rshift))
             (setf (first *edit-mode-cursor-positions*)
                   (edit-mode-move-line (first *edit-mode-cursor-positions*) -1))
             (edit-mode-cursor-positions-fix))
            (t
             (setf (edit-mode-cursor-positions)
                   (edit-mode-move-line (first *edit-mode-cursor-positions*) -1))))
          t)
         (:down
          (cond
            ((intersection *held-keys* '(:lshift :rshift))
             (setf (second *edit-mode-cursor-positions*)
                   (edit-mode-move-line (second *edit-mode-cursor-positions*) 1))
             (edit-mode-cursor-positions-fix))
            (t
             (setf (edit-mode-cursor-positions)
                   (edit-mode-move-line (second *edit-mode-cursor-positions*) 1))))
          t))))))