
(in-package #:cowl)

(compiler-opts)

(defvar *root-widget* nil
  "Cowl widgets are layed out in grid containers,
this represents the root of the tree. TODO say more")

(defvar *font* nil
  "The default font used for text widgets.")

(defparameter *default-font-size* 12)

(defun delete-default-font ()
  (when *font*
    (ftgl:destroy-font *font*)
    (setf *font* nil)))

(defun default-font ()
  (unless *font*
    (setf *font* (ftgl:create-texture-font))
    (ftgl:set-font-face-size *font* *default-font-size* 72))
  *font*)

(defun (setf default-font) (font)
  (delete-default-font)
  (setf *font* font))

;;; INPUT STATE

(defparameter *focused-widget* nil
  "The currently active input widget. If nil, nothing is currently being edited.")
(declaim (type (or t null) *focused-widget*))

(defparameter *ignore-events* nil
  "This must be set to T before any event processing is done.
The reason this is here is to prevent superfluous events from
firing during application startup before the system is ready.")
(declaim (type boolean *ignore-events*))

(defparameter *prev-mouse* (cons 0 0)
  "Cons pair (x . y) of the last mouse position.")

(defparameter *held-keys* nil
  "A list of currently held keys, characters or keywords.")


;;;; GENERIC FUNCTIONS FOR INPUT

(defgeneric handle-mouse-button (widget button press x y)
  (:documentation "When a widget needs to respond to a click, implement this.
The x and y arguments are the relative location to the widget.")
  (:method (widget button press x y)))

(defgeneric handle-mouse-wheel (widget zrel x y)
  (:documentation "When a widget needs to respond to wheel motion, implement this.
The x and y arguments are the relative location to the widget.")
  (:method (widget zrel x y)))

(defgeneric handle-mouse-motion (widget x y)
  (:documentation "When a widget needs to respond to mouse motion, implement this.
The x and y arguments are the relative location to the widget.
It's probably best, however, if you register a function on *mouse-motion-handlers*
only when required.")
  (:method (widget x y)))

(defgeneric handle-key (widget key press)
  (:documentation "When a widget needs to respond to a key press/release when it
is focused, implement this.")
  (:method (widget key press)))

(defgeneric update-text (widget)
  (:documentation "Calls all the text updater methods in all the descendents of widget.")
  (:method (widget)))

(defgeneric accept-focus-p (widget)
  (:documentation "Whether this widget will accept focus or not. Returns boolean.")
  (:method (widget)))

(defgeneric blur (widget)  
  (:documentation "Called just before this widget is un-focused;
ie. *focused-widget* will still be bound to widget when it is called.")
  (:method (widget) "Default no-op" nil)
  (:method :after (widget)
    "Clear the *focused-widget* parameter, after main actions."
    (when (eql *focused-widget* widget)
      (setf *focused-widget* nil))))

(defgeneric focus (widget)
  (:documentation "Focus this widget. Call to place this widget in focus.
Implement as a main method to determine actions to be taken just 
after *focused-widget* is set to widget.")
  (:method :around (widget)
    "Ensure that we can accept focus and defocus whatever is currently in focus before widget is focused."
    (when (accept-focus-p widget)
      (when *focused-widget*
        (blur *focused-widget*))
      (setf *focused-widget* widget)
      (call-next-method))))


;;; EARLY  EVENT HANDLERS

;; List handlers for various event types, these are triggered first until one returns t
(defparameter *mouse-button-handlers* nil
  "List of functions triggered on a mouse button press/release event. Return t if event was handled, otherwise nil.
Arguments passed are: button (keyword symbol :left, :middle, :right), press (t (press) or nil (release)), x, y (integer position of mouse).")
(defparameter *mouse-motion-handlers* nil
  "List of functions triggered on a mouse motion event. Return t if event was handled, otherwise nil.
Arguments passed are: x, y (integer position of mouse).")
(defparameter *mouse-wheel-handlers* nil
  "List of functions triggered on a mouse wheel event. Return t if event was handled, otherwise nil.
Arguments passed are: zrel (integer delta of mouse wheel 'position'), x, y (integer position of mouse).")
(defparameter *key-handlers* nil
  "List of functions triggered on a key press/release event. Return t if event was handled, otherwise nil.
Arguments passed are key (keyword symbol or character), press (t (press) or nil (release)).")


;;; TOP-LEVEL EVENT HANDLERS

(defun key-event (key press)
  "Send a key event to cowl. Key is either a character type or a keyword of the key.
Action should either be T for a press or NIL for a release."
  (declare (type (or character symbol) key))
  (declare (type boolean press))
  (unless *ignore-events*
    (if press
        (pushnew key *held-keys*)
        (setf *held-keys*
              (delete key *held-keys*)))
    (unless (some #'(lambda (h)
                      (funcall h key press))
                  *key-handlers*)
      (case key
	(:tab
	 (when press
	   (let (w)
	     (if (find-if (lambda (hk) 
			    (or (eql hk :lshift)
				(eql hk :rshift)))
			  *held-keys*)
		 (progn
		   (when *focused-widget*
		     (setf w (prev-widget *focused-widget*)))
		   (unless w
		     (when *root-widget*
		       (setf w (find-last-focusable *root-widget*)))))
		 (progn
		   (when *focused-widget*
		     (setf w (next-widget *focused-widget*)))
		   (unless w
		     (when *root-widget*
		       (setf w (find-first-focusable *root-widget*))))))
	     (when w 
	       (focus w)))))
	(otherwise
	 (when *focused-widget*
	   (handle-key *focused-widget* key press)))))))


(defun mouse-button-event (button press &optional (x (car *prev-mouse*)) (y (cdr *prev-mouse*)))
  "Send a new mouse button event into Cowl. 
This should be called on press/release.
Button should be one of :LEFT, :MIDDLE, or :RIGHT.
x and y are the current mouse coordinates; 
if they are unspecified, the last value given to mouse-motion-event will be used."
  (declare (type symbol button))
  (declare (type boolean press))
  (declare (type integer x y))
  (unless *ignore-events*
    (unless (some #'(lambda (h)
                      (funcall h button press x y))
                  *mouse-button-handlers*)
      (when *root-widget*
       (handle-mouse-button *root-widget* button press
                            (- x (current-offset-of (x *root-widget*)))
                            (- y (current-offset-of (y *root-widget*))))))))

(defun mouse-wheel-event (zrel &optional  (x (car *prev-mouse*)) (y (cdr *prev-mouse*)))
  "Send a new wheel event. zrel is the difference from the last event of the
mouse wheel position. Nominally this would be 1 or -1. 
x and y are the current mouse coordinates; 
if they are unspecified, the last value given to mouse-motion-event will be used."
  (declare (type integer zrel))
  (declare (type integer x y))
  (unless *ignore-events*
    (unless (some #'(lambda (h)
                      (funcall h zrel x y))
                  *mouse-wheel-handlers*)
      (when *root-widget*
       (handle-mouse-wheel *root-widget* zrel
                           (- x (current-offset-of (x *root-widget*)))
                           (- y (current-offset-of (y *root-widget*))))))))

(defun mouse-motion-event (x y)
  "Send a new mouse position into Cowl.
This should be hooked up typically to the windowing system, with coordinates relative to the
top-left of the viewport. This means, for some mouse position generators, you will need to 
invert the y axis, eg. (- viewport-height y 1)."
  (declare (type integer x y))
  (prog1 (unless *ignore-events*
           (some #'(lambda (h)
                     (funcall h x y))
                 *mouse-motion-handlers*))
    (setf (car *prev-mouse*) x
          (cdr *prev-mouse*) y)))


;;; TOP-LEVEL RENDER 

(defun layout-root (&optional (root-widget *root-widget*))
  (layout *root-widget*))

(defun draw-root (&optional (root-widget *root-widget*))
  "This is the top-level method to render a cowl widget sheet.
It will start with *root-widget*, unless an argument is supplied, which will nominally be a container."
  (destructuring-bind (width height)
      (let ((viewport (list 0 0 0 0)))
        (gl:get-integerv gl:+viewport+ viewport)
        (cddr viewport))
    (gl:with-push-attrib (gl:+current-bit+ gl:+lighting-bit+ gl:+texture-bit+ gl:+depth-buffer-bit+ gl:+polygon-bit+)
      (gl:disable gl:+lighting+)
      (gl:disable gl:+depth-test+)
      (gl:depth-mask gl:+false+)
      (gl:enable gl:+texture-2d+)
      (gl:shade-model gl:+flat+)
      (gl:polygon-mode gl:+front-and-back+ gl:+fill+)
      (gl:with-projection-matrix 
          ((glu:ortho-2d 0 width height 0))
        (gl:with-push-matrix
          (gl:load-identity)
          (gl:translate-f (current-offset-of (x root-widget)) 
                          (current-offset-of (y root-widget))
                          0.0)
          (draw root-widget))))))

