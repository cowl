(defpackage #:cowl-system
  (:use #:asdf #:cl))

(in-package #:cowl-system)

(defsystem #:cowl
  :name "cowl"
  :author "William Robinson <airbaggins@users.sf.net>"
  :version "0.1"
  :description "Common Lisp OpenGL(TM) Widget Library"
  :licence "MIT"
  :depends-on (#:cffi #:cl-glfw-ftgl #:cl-glfw-glu #:cl-glfw-opengl-version_1_1 #:trivial-garbage)
  :components ((:module
                src
                :serial t
                :components
                ((:file "cowl-package")
                 (:file "cowl-font")
                 (:file "cowl")
                 (:file "cowl-widget")
                 (:file "cowl-container")
                 (:file "cowl-text")
                 (:file "cowl-graph")
                 (:file "cowl-viewport")
                 (:file "cowl-progress")
                 (:file "cowl-button")
                 (:file "cowl-slider")
                 (:file "cowl-input")
                 (:file "cowl-numeric-input")
                 (:file "cowl-style")
                 (:file "cowl-convenience")
                 (:file "cowl-file-selector")))))
