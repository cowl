(defpackage #:asdf-cpplib
  (:use #:asdf #:cl)
  (:export #:system-split #:cpp-library))

(in-package #:asdf-cpplib)


;;; PARAMETERS
(defvar *c++-compiler* "c++")
(defvar *c++-flags* '("-fPIC" "-O2" "-W" "-Wall" "-shared"))
(defvar *libs* '())

;;;; UTILITY
(defun system (program args &key (echo nil))
  (flet ((quotify (a)
           (if (find-if #'(lambda (c)
                            (find c '(#\Space #\Newline #\Return #\Linefeed #\Tab)))
                        a)
               (format nil "~s" a)
               a)))
    (when echo (format t "~a ~{~a~^ ~}~%"
                       (quotify program)
                       (mapcar #'quotify args))))
  #+sbcl
  (with-output-to-string (out)
    (sb-ext:run-program program args :wait t :search t :output out :error t :input nil)))

(defun string-split (string &optional (test #'(lambda (c) (find c '(#\Newline #\Space #\Page #\Tab #\Return #\Linefeed)))))
  (loop for i = 0 then (1+ j)
     as j = (position-if test string :start i)
     collect (subseq string i j)
     while j))


;;;; ASDF EXTENSIONS
(defclass cpp-library (asdf:source-file)
  ((extra-cflags :initarg :extra-cflags :initform nil :reader extra-cflags-of)
   (extra-libs :initarg :extra-libs :initform nil :reader extra-libs-of)))

(defmacro define-extra-param (name)
  (let ((extra-of (intern (format nil "EXTRA-~a-OF" name)))
        (extra (intern (format nil "EXTRA-~a" name))))
    `(progn
       (defmethod (setf ,extra-of) (val (cpp-library cpp-library))
         ;; (format t "Setting extra ~a to ~s~%" ',name val)
         (with-slots (,extra) cpp-library
           (etypecase val
             (null (setf ,extra nil))
             (cons (setf ,extra val))
             (symbol (setf (,extra-of cpp-library)
                           (symbol-value val)))
             (string (setf (,extra-of cpp-library) 
                           (remove-if #'(lambda (s)
                                          (zerop (length s)))
                                      (string-split val))))))))))

(define-extra-param cflags)
(define-extra-param libs)

(defmethod initialize-instance :after ((cpp-library cpp-library)
                                       &key (extra-cflags nil) (extra-libs nil) &allow-other-keys)
  (setf (extra-cflags-of cpp-library) extra-cflags
        (extra-libs-of cpp-library) extra-libs))

(defmethod reinitialize-instance :after ((cpp-library cpp-library)
                                       &key (extra-cflags nil) (extra-libs nil) &allow-other-keys)
  (setf (extra-cflags-of cpp-library) extra-cflags
        (extra-libs-of cpp-library) extra-libs))


(defmethod source-file-type ((c cpp-library) (s module)) "cpp")

(defmethod perform ((o load-op) (c cpp-library))
  #+sbcl
  (sb-alien:load-shared-object (namestring (first (input-files o c))))
  
#|  (funcall (find-symbol "LOAD-FOREIGN-LIBRARY" (find-package '#:cffi))
           (list* :or (mapcar #'(lambda (p)
                                  (make-pathname :type nil :defaults p))
                              (input-files o c))))|#
)

(defmethod perform ((o compile-op) (c cpp-library))
  (system *c++-compiler*
          (append *c++-flags*
                  (extra-cflags-of c)
                  (list "-o" (namestring (first (output-files o c))))
                  (mapcar #'namestring (input-files o c))
                  (extra-libs-of c))
          :echo t))

(defmethod input-files ((o compile-op) (c cpp-library))
  (list (component-pathname c)))

(defmethod output-files ((o compile-op) (c cpp-library))
  (list
   (make-pathname :type
                  #+unix "so"
                  #+windows "dll"
                  #+darwin "dylib"
                  :defaults (component-pathname c))))