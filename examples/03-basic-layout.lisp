(require '#:asdf)
(asdf:oos 'asdf:load-op '#:cowl-glfw)

(defun main ()
  (glfw:do-window (:title "Cowl Basic Layout" :width 400 :height 300)
      ((setf cowl:*root-widget*
             ;; this is basically a wrapper around the generic cowl:container
             (cowl:make-vbox (:width 400 :height 300
                              :row-heights '((:weight . 1.0)
                                             (:expand . 10)
                                             (:expand . 10)
                                             (:weight . 1.0)))

               ;; the body of this macro is a list of widgets for the wrapper

               nil ;; nil in weighted spaces will expand as necessary

               (cowl:make-label "Hello, basic layout.")

               (cowl:make-hbox (:width 400)
                 (cowl:make-button () "One"
                   (format t "Button One clicked!~%"))
                 (cowl:make-button () "Two"
                   (format t "Button Two clicked!~%"))
                 (cowl:make-button () "Three"
                   (format t "Button Three clicked!~%"))
                 (cowl:make-button () "Four"
                   (format t "Button Four clicked!~%")))
               

               (cowl:make-button () "Click to exit!"
                 ;; The body of this macro is executed on left-click.
                 (return-from main))

               nil))

       (cowl-glfw:setup-input-callbacks))
     
    ;; Main loop the same as before
    (gl:clear gl:+color-buffer-bit+)
    (cowl:layout-root)
    (cowl:draw-root)
    (sleep 0.04)))

(main)
