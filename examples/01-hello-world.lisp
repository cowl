(require '#:asdf)

;; this will load cowl, glfw and their bridging events layer (even though we
;; don't ACTUALLY need it here, it's just convenient get them in one line).
(asdf:oos 'asdf:load-op '#:cowl-glfw)

(defun main ()
  ;; High-level convenience macro from cl-glfw.
  (glfw:do-window (:title "Cowl Hello World!" :width 400 :height 300)
      ;; This is the init code, run once after the OpenGL context comes up.
      ((setf cowl:*root-widget* (cowl:make-label "Hello world!" :x 100 :y 100))
       (gl:enable gl:+blend+)
       (gl:blend-func gl:+src-alpha+ gl:+one+))

    ;; And the rest is the main loop.
    (gl:clear gl:+color-buffer-bit+)	; wipe the display

    (cowl:layout-root)                  ; compute the layout
    (cowl:draw-root)                    ; render the widgets
    
    ;; don't take up all the CPU
    (sleep 0.04)))

(main)
