(require '#:asdf)
(asdf:oos 'asdf:load-op '#:cowl-glfw)




(defclass cowl-matrix ()
  ((container :type cowl:container :accessor widget-of)
   (matrix :type array
           :initform (make-array '(4 4) :element-type 'single-float
                                 :initial-contents '((1.0 0.0 0.0 0.0)
                                                     (0.0 1.0 0.0 0.0)
                                                     (0.0 0.0 1.0 0.0)
                                                     (0.0 0.0 0.0 1.0)))
           :accessor matrix-of))
  (:documentation "This is a graphical representation of a matrix."))

(defmethod initialize-instance :after ((cowl-matrix cowl-matrix) &key)
  (setf (widget-of cowl-matrix)
        (cowl:make-grid ()
          ((cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 0 0))
           (cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 0 1))
           (cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 0 2))
           (cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 0 3)))
          ((cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 1 0))
           (cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 1 1))
           (cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 1 2))
           (cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 1 3)))
          ((cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 2 0))
           (cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 2 1))
           (cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 2 2))
           (cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 2 3)))
          ((cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 3 0))
           (cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 3 1))
           (cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 3 2))
           (cowl:make-scroll-button () "~4f" (aref (matrix-of cowl-matrix) 3 3))))))


(defun multiply-matrix (a b r)
  "Multiply matrix a by b and put result in r."
  (loop for row below 4
     do (loop for col below 4 
           do (setf (aref r row col)
                    (loop for i below 4 summing
                         (* (aref a row i)
                            (aref b i col)))))))


(defun main ()
  (glfw:do-window (:title "Cowl Matrix Example" :width 800 :height 600)
      ((setf cowl:*root-widget*
             ;; here we have our bindings to the matrices so we can close over them
             (let (ma mb mr)
               (cowl:make-vbox (:width 800 :height 600
                                       :row-heights '((:weight . 1.0)
                                                      (:expand . 10)
                                                      (:expand . 10)
                                                      (:expand . 10)
                                                      (:weight . 1.0)))

                 ;; the body of this macro is a list of widgets for the wrapper

                 nil ;; nil in weighted spaces will expand as necessary

                 (cowl:make-label "Matrix multiplication example")

                 (cowl:make-hbox (:width 800
                                  :column-widths '((:weight . 1.0)
                                                   (:expand . 10)
                                                   (:weight . 1.0)
                                                   (:expand . 10)
                                                   (:weight . 1.0)
                                                   (:expand . 10)
                                                   (:weight . 1.0)
                                                   (:expand . 10)
                                                   (:weight . 1.0)
                                                   (:expand . 10)
                                                   (:weight . 1.0)))
                   nil
                   (widget-of (setf ma (make-instance 'cowl-matrix)))
                   nil
                   (cowl:make-label "x")
                   nil
                   (widget-of (setf mb (make-instance 'cowl-matrix)))
                   nil
                   (cowl:make-label "=")
                   nil
                   (widget-of (setf mr (make-instance 'cowl-matrix)))
                   nil)

                 (cowl:make-button () "Calculate"
                   (multiply-matrix (matrix-of ma)
                                    (matrix-of mb)
                                    (matrix-of mr))
                   (cowl:update-text (widget-of mr)))
               

                 (cowl:make-button () "Quit"
                   (return-from main))

                 nil)))

       (cowl-glfw:setup-input-callbacks))
     
    ;; Main loop the same as before
    (gl:clear gl:+color-buffer-bit+)
    (cowl:layout-root)
    (cowl:draw-root)  
    (sleep 0.04)))

(main)
