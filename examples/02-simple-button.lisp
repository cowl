(require '#:asdf)
(asdf:oos 'asdf:load-op '#:cowl-glfw)

(defun main ()
  (glfw:do-window (:title "Cowl Simple Button" :width 400 :height 300
			  :opengl-version-major 2
			  :opengl-version-minor 0)
      ((setf cowl:*root-widget*
             (cowl:make-button (:x 100 :y 100) "Click to exit!"
               ;; The body of this macro is executed on left-click.
               (return-from main)))

       ;; Attach the cowl input callbacks to GLFW.
       ;; Note that this will wipe out your own input callbacks,
       ;; if any are already set up to GLFW.
       (cowl-glfw:setup-input-callbacks))
     
    ;; Main loop the same as before
    (gl:clear gl:+color-buffer-bit+)
    (cowl:layout-root)
    (cowl:draw-root)
    (sleep 0.04)))

(main)
