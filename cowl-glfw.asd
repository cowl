(defpackage #:cowl-glfw-system (:use #:cl #:asdf))
(in-package #:cowl-glfw-system)

(defsystem #:cowl-glfw
  :description "Event handlers for connection GLFW to Cowl."
  :depends-on (#:cowl #:cl-glfw)
  :components ((:module src
                        :components ((:file "cowl-glfw")))))
